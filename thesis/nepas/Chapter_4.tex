 \chapter{NePAS Framework Design}
\label{chap:chapter_4}

This section discusses the design consideration of a system that will be used to evaluate the abstraction techniques proposed for various network policies called Network Policy Abstraction System (NePAS).

NePAS is a framework that is used in compiling and deploying conflict free vendor-specific configuration from abstract high level network policy intentions and associated network topology graphs. The graphs that are generated for the high-level policy intentions are designed to be independent of the network layout devices. Likewise the graphs generated for the representation of the proposed network layout are independent of vendor-platform that will be used for compiling low-level configuration commands. Both the policy intention and network layout graphs are produced using graphical editors such as yED. The graphs to be generated use nodes and edges to express both the intended policy consideration and network layouts. The NePAS framework was developed using template design approach discussed in chapter \ref{chap2:template}. NePAS uses a combination of network policy template snippets and NIDB gotten from the various in order to generate Cisco VIRL configuration files that can be used to deploy the proposed network in Cisco VIRL. The NePAS framework is designed around five phases as follows:

\begin{itemize}  \itemsep1pt \parskip0pt \parsep0pt
		\item \emph{Phase I - Policy Intention}: the first phase of NePAS is where the network policy intention is described independent of the network topology layout. Inter-domain routing policy intention, firewall access policy intention, cyber security exercise intentions are expressed in an abstract, simple, and easy manner using graphs in this case.
		
		\item \emph{Phase II - Network Layout}: the second phase of NePAS is where the actual layout of the proposed network is represented in a vendor-independent manner using graphs. The various network policies designed in Phase I can then be assigned to various network devices in this phase of NePAS.
			
		\item \emph{Phase III - Anomaly Resolution}: the third phase of NePAS is where the network layout devices with their respective high level policy intentions are passed through various network anomaly resolution algorithms before low level configuration commands are compiled. This is to ensure the final deployments are conflict free and behave as intended by network administrators. 
		
		\item \emph{Phase IV - Compilation}: the fourth phase of NePAS is where a Cisco VIRL XML file with a set of conflict free vendor-specific device-centric configuration commands for all the devices of the proposed network are generated.
		
\end{itemize}

\begin{figure}[H]
\begin{minipage}[b]{1.1\linewidth}
\centering
\includegraphics[width=\linewidth]{nepas/framework.eps}
\caption{NePAS Framework}
\label{fig:frame}
\end{minipage}
\end{figure}

Figure \ref{fig:frame} above illustrates the sequencing of phases of the system proposed in this thesis. A combination of high level network policy intention and a vendor-independent network layout are processed using template design approach so as to automatically generate Cisco VIRL configuration files that can then be deployed in VM Maestro workspaces. The following section provides and in-depth discussion of the design of the various phases of NePAS.

  
  
  


\section{Policy Intention Abstraction Phase}

This is the first phase of NePAS where the network policy intention of the proposed network is expressed. At this level of abstraction, network devices such as routers, firewalls, switches, and many other middle-boxes that are used in enterprise networks are in contention. NePAS is able to abstract that level of detail so as to give a network administrator the freedom of representing various network policy intentions such as firewall relationships, BGP routing policies or cyber security exercise approaches using graphical editors without thinking about how the actual layout of the network will look like or the type of devices that will be used for the proposed experiment. The following are the two major aspects used in specifying any type of network intention policy implemented by NePAS in this phase. 


\subsection{Realms}
The nodes of a policy intention graph are used by NePAS to represent realms which can either be a single network device (such as a servers, firewalls, routers, etc.) or a group of devices that form a network (such Autonomous Systems, CDX teams or devices with identical access control policies) as the case maybe. It should be noted that a device or group of devices within a realm must have identical network policy intentions. Realms need to be labeled as their labels will be useful in the network layout phase of NePAS to be discussed in the following section of the thesis. The only reserved NePAS label for realms in a policy intention graph is the keyword 'any'. This label, \emph{any}, has been reserved for expressing firewall relationships in NePAS and can only be used in that context. The use of the keyword 'any' for a purpose other than firewall relationships will be ignored by NePAS. Detailed explanation of the usage of the keyword \emph{any} will be covered in Chapter \ref{chap6:fwpolabs}. There are additional details needed to fully represent realms in a network policy intentions graph of a proposed network such details are referred to as realm options. 
\textit{Realm Options} - are policy details that will be used on the nodes (or realms) of a given network policy intention graph to further express additional details of the proposed network experiment. For example a realm in a BGP network policy intention graph can have custom optional values that will allow a user to specify the autonomous system number. These custom attributes can either have default values while in other cases it is imperative to declare a specific value. This is to enable NePAS to abstract a lot of network policy specification details while providing a very flexible setup so as to express various levels of network abstractions.
  

\subsection{Relationships}
The edges that connect realms are used by NePAS to represent the network policy relationship between the two associated realms. The labels on the edges between realms state the exact network policy relationship between the two realms. For example in firewall intentions, the edges are either accept or deny to reflect the action that will be taken by the firewall rule. Edges at this level of abstraction do not have the typical default values that are popular with network graph abstraction which activates a default value if details have been omitted on edges in the graph. This is because NePAS uses the label on edges between realms to know what type of network policy relationship it is dealing with during the compilation process. Hence if any edge label is omitted intentionally or unintentionally, an error message will be generated automatically. There are additional details needed to fully represent realm relationships in the network policy intentions graph of a proposed network. The additional details are called edge or relationship options.
\textit{Edge Options} - are policy details that are used on the edges (or relationships) of a given graph to further express additional details of the network policy intention. Some of these details such as the destination port of a firewall rule which represents the port numbers or service names can be abstracted and expressed in a very easy manner for the network administrator in the form of custom properties. For example, service with value \textit{www} in a given firewall relationship can be used to represent web server running on port 80. These custom attributes can either have default values while in other cases it is imperative to declare a specific value. This is to enable NePAS to abstract network policy specification details while providing a very flexible setup so as to express various network policies.

  
 




\section{Network Layout Abstraction Phase}

This is the second phase of NePAS where the actual network infrastructure devices of the proposed network are expressed in a vendor independent manner. The network layout is expressed in a graphical format in a way similar to the policy intention detailed in the previous section. The abstraction at this level is similar to current automated configuration systems that use graphical editors for their input such as Autonetkit amongst many others. It should be noted that the abstractions proposed in this phase allow network administrators to express re-usable blocks and network slices as stated in the previous chapter. The level of abstraction in this phase of NePAS is lower than the policy intention abstraction as routers, switches and other network devices are featured here. The network policies defined in the previous section will be assigned to the various network devices as the proposed network dictates in this phase of NePAS. It should be noted that low level configurations such as IP addressing can still be abstracted at this level depending on the requirements of the proposed network. In this phase of NePAS, nodes within a graph are used to represent various network devices such as routers, firewalls, servers, switches and many others. The edges of a network layout graph are used to specify the logical structure (or link) of the connected network devices.






\subsection{Network Devices} 
\label{nepas_nd}
The nodes of a network layout graph in this phase are used to represent network devices such as routers, firewalls and servers amongst others. The label of a node in the network layout graph is used to denote the name of the network device during the low level configuration process. The nodes used to express various network devices in this phase are designed to have node options for additional properties. Node options are essential for expressing the network device and the policy group a node belongs to in the network layout abstraction phase of NePAS. There are three node options that have been provided for devices during the expression of the network layout in NePAS. 
These node options are as follows: 
(i) the type of network device the node represents within the graph
(ii) the firewall realm the network device belongs to from the policy intention graph supplied
(iii) the BGP realm a router belongs to in the policy intention graph supplied.   


The first node option used as a custom property for network devices in the network layout graph is called \textbf{\emph{dtype}}. This node option is used in specifying the type of network device NePAS will be dealing with. The optional  a device type can take include server, router, kali and many others. The device type selected influences the disk image to be used during the low level configuration process as some servers have applications off until activated at this stage by selecting the appropriate node option. A very good example here are servers selected with various vulnerabilities during a cyber security competition. The option selected at this point also influences the type of network interface type to be used for low level configuration of the various device types in the network. The interface used by servers is different from the ones used by network devices such as routers or firewalls.


The second node option used as a custom property for network devices is called \textbf{\emph{fwpol}}. This option is designed to indicate the firewall policy intention realm a network device belongs to in the policy intention graph. The value selected in this phase is used in configuring the firewall policies of the various network devices in the proposed network. It should be noted that devices such as switches, firewalls and routers are not expected to have firewall policies. Hence NePAS has been designed to ignore firewall policies assigned to such devices during low level configuration command compilation. A detailed description of how this node option is to be implemented is carried out in chapter \ref{chap6:fw} of this thesis.


The third and last node option used as a custom property designed in this phase for network routers is called \textbf{\emph{bgppol}}. This option has been designed to indicate the BGP policy intention realm the network router belongs to in the policy intention graph. This option determines the autonomous system a router belongs and the type of BGP relationship it has with other routers in the proposed network as expressed in the policy intention graph. It should be noted that only routers are designed to have this option and if a server or switch is assigned this value, NePAS will ignore it during processing. A detailed description of how this node option is to be implemented is carried out in chapter \ref{chap5:bgp} of this thesis.  
 


\subsection{Links} 
The edges of a network layout graph are used to specify the logical structure between the two connected network devices. The edges of a network layout graph do not make use of its label and hence will be ignored if specified during the low level configuration process. The edges between two nodes in a network layout graph has been designed with two custom edge options. These set of options have to do with specification of low level IP addressing for the network devices connected by the edge. This is so as to give network administrators greater abstraction flexibility when deploying a proposed network. The two edge options designed in this phase can either be manually assigned or generated and allocated to connected devices are called \textbf{\emph{sIP}} and \textbf{\emph{dIP}}. The two options are designed by default to take 'NULL' values and hence a pair of IP addresses will be generated automatically. 






\section{Network Anomaly Resolution Phase}
This is the third phase of NePAS where the policy intention and network layout graphs are combined and passed through various anomaly resolution algorithms. This phase is designed to resolve anomalies that arise after the low level configuration process. During the merger of the policy intention and network layout graphs, a number of anomalies can come up that will impact the final deployment of the proposed networks. The various network policy anomaly resolution algorithms designed in this phase ensure final networks deployed do not suffer issues of network connectivity, performance and security lapses amongst others. Network policy anomalies that are handled are those that occur during the generation of network configuration commands. Anomalies that occur after the proposed network has been deployed due to network changes are out the scope of our research and are not handled. 








\section{Compilation Phase}
The fourth and last phase of NePAS is designed to generate a set of vendor specific policy configuration commands for all devices of the proposed network. NePAS is designed to use template design approach to generate the low-level vendor specific configuration commands that will be used for configuring the devices of the proposed network with their respective network policies supported by NePAS. Using the template design approach of automated configuration, a number of snippets have been developed for NePAS to generate the appropriate low-level configuration commands of the network policies for the various devices within the proposed network. The configuration snippets will be using the output from the anomaly resolution section such as IP addresses and also handles the ordering of filters so as to generate consistent and anomaly-free low-level configuration commands for the proposed network. Finally, NePAS will generate an XML file that can be easily exported into Cisco VIRL for the deployment of the proposed experiment. The file generated is designed to be used for deployment of the proposed network in Cisco VIRL with configuration snippets for the high level network policy intentions. 

During the compilation process, NePAS generates a number of log messages that is used to show what is going on behind the scene and reporting the progress. These log messages can be broadly categorised into three groups: information messages; warning messages and error messages. 






\section{Closing Remarks}
This chapter of the thesis gives a detailed overview of the system developed to evaluate the limitation of the network policy abstractions that are proposed for our research project. The system developed is composed of four phases: policy intention; network layout; anomaly resolution and compilation. The next chapter will be used to introduce the concept behind inter domain routing protocol, Border Gateway Protocol (BGP). The chapter will move on to discuss the network policy abstraction that we have developed for BGP business relationships.



