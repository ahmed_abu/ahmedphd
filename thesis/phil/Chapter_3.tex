\chapter{Philosophy of Network Abstractions}
\label{chap:chapter_3}

% should it be Thoughts on network abstractions

\section{Introduction}
A solution to computer network complexity especially when trying to simplify the management process problem is abstraction. Abstraction can make a seemingly complex network less so by removing or reducing details such as network devices, network services, few protocols and anything that can be removed without tempering with the behaviour of the network once deployed. Network abstraction models should be able to deploy high level policy intentions that matches down into low level vendor specific configuration commands easily. The low level configuration commands generated from the abstract representation of the network specified should be conflict free and behave as intended by the network administrators. Abstraction can also greatly simplify and minimise errors in  the network management process when updating deployed enterprise networks. For example, a user can change or update a single firewall intention that reverberates across a large set of network devices.
This chapter is broken into four major subsection as follows: 


\begin{itemize}

\item Section \ref{datastruct} of this chapter is used to examine the various ways network information can be obtained from administrators and why graph theory was preferred over other data structures in this research.

\item Section \ref{deployment} of this chapter is used to state the deployment environment of proposed experimental network models.  

\item  Section \ref{polintsec} of this chapter showcases how computer network policies can be abstracted by removing details such as the network devices in the proposed layout to be deployed. 
The abstractions proposed in this section have no bearing the network layout and/or devices that such policies will be implemented on during deployment. Inter-domain (BGP) routing policies, firewall intention policies and cyber security infrastructure relationships will be introduced in this section, with more in depth discussions presented in Chapter \ref{chap5:bgp} ,  \ref{chap6:fw} and \ref{chap:chapter_7} respectively.

\item Section \ref{netlayabs} of this chapter showcases how the network layout of a proposed experiment can be abstracted either as re-usable blocks (or collection of devices) or slices (cookie dough) of network layouts as the case maybe. It should be noted that the abstractions presented in this section remove details such as the vendor product being used, IP addresses and others from the network administrator.
\end{itemize}




\section{Graph Theory}
 \label{datastruct}
This chapter of the research conducted hopes to give a philosophical background of how abstractions based on template design approach be used to simplify and/or improve the network management process for network administrators. The following section will be used in showcasing how our proposed abstraction methodologies can be implemented in order to achieve the aim of simplifying the network management process. 

There are multiple ways in which the network information of a proposed experiment can be gotten from a network administrator, these include amongst many others data structures, sets, mathematical algebraic expressions and graph theory. All these and many others can be used to get details of a network experiment from an administrator. 
Graph theory is the study of graphs which are used as a way to formally represent networks or inter-connected devices. The inner-workings of graph theory is beyond the scope of work conducted in this research and hence will not discussed in-depth. The graph in Figure \ref{fig:graph} below shows a basic graph with two nodes labelled A and B. The nodes (also called vertices) are connected using an edge (also called link) in the diagram below. 

\begin{figure}[htbp]
\begin{minipage}[b]{0.8\linewidth}
\centering
\includegraphics[width=\linewidth]{phil/graph.eps}
\caption{A Basic Graph}
\label{fig:graph}
\end{minipage}
\end{figure}

We use the nodes of a graph similar to the example in Figure\ref{fig:graph} above to represent either a collection of network devices (realms) or an individual network device. The edges of the graph in this research is used to represent the relationship between two realms or as a link between two network devices. 
Graph theory have been adopted in this research for expressing both high level network policy intentions and the intended network layout of any given experiment. 
An overview of how graph theory was used to abstract the network management process will be conducted in this chapter and a more detailed discussion of it will be carried out in subsequent chapters. 

The abstractions proposed in this research will be adopted in graph-like formats (using graph theory) similar to white board representations in order to simplify and make the network management process easier to showcase and understand. The proposed network abstractions presented in this research will be based on template design approach discussed in Chapter \ref{chap2:template}. Graphs will be used to abstract network policy intention and layout information of experiments which will be parsed to template snippets in order to generate low level configuration commands. There are two types of graphs used for the proposed abstractions in this research, these are: policy intention and network layout graphs. The first graph type is used to express high level network policy intention abstractions that are independent of the proposed network layout of the experiment. The second graph type is used to express an abstraction of the network layout that is independent of the network device vendor such as Juniper or Cisco. The nodes and edges of the graphs have custom properties that are used in populating the NIDB of the proposed network. A more detailed and comprehensive discussion of the graph and properties in them will be carried out in the next Chapter \ref{chap:chapter_4}.  This chapter will be looking at network abstraction from two angles: network policy intention level and network layout level. The policy intention level of network abstraction is the highest level where the relationships between group of devices with identical intentions are specified. The network layout level of network abstraction is on a lower level than the policy intention and deals with how network layouts of proposed experiments can be simplified and expressed. 






\section{Deployment}
\label{deployment} 
The progression of the research work conducted in this thesis is to propose high level network policy abstractions that will implemented in a virtualised environment and then move on to real work or physical hardware deployments. There is a number of issues that manifest with the deployment of networks in the real world such as BGP wedgies (discussed in the next chapter) that are beyond the scope of work conducted. Also, BGP policies are implemented based on multiple service provider agreements that are opaque and not readily available to individuals outside the organisation. This makes configuring BGP policies in the real world much more complex for the various network administrators managing those networks. All the work carried out in this thesis is therefore envisaged to be deployed in a controlled virtualised environment. 







\section{Policy Intention Level Abstraction}
\label{polintsec}
This is the first part of our philosophical research contribution on network abstraction. This section will be focusing on the highest level of the network abstraction process, network policy intention abstraction. This level of network abstraction sought to view the network management process from a policy specification angle. As stated in chapter \ref{chap:Introduction} of this research, network policies are a collection of rules used to govern the connectivity, routing and/or access requirements of the devices within a proposed network layout. At this level of abstraction, the network layout or infrastructure devices are not in contention or specified. The network policy of devices with identical behaviour are grouped together expressed in this section. Two network policies dealing with network routing (BGP) and access control (firewall) will be introduced in this section. It should be noted that a more comprehensive discussion of the network abstraction process for network inter-domain routing and access control will be detailed in Chapter \ref{chap5:bgp} and \ref{chap6:fw} of this research respectively.

\subsection{BGP Abstractions}
This section will be giving a brief overview at our proposed high level inter domain routing intention abstractions. A comprehensive review of BGP and what it entails is be discussed in Chapter \ref{chap5:bg}. However, a brief introduction regarding BGP will be given in the following section.

Border gateway protocol, BGP, is an inter domain routing protocol that is used in selecting the best route to any given destination; advertising reachability of prefixes; and accepting routes from neighbouring internet service providers (ISP) or autonomous systems. An autonomous system is a network or group of networks whose prefixes and routing policies are under a common administration. The following are some of the BGP business relationships that can be implemented between adjoining ISPs.

\begin{itemize}
  \item \textbf{Peer relationship} - This relationship occurs when two or more similar sized autonomous systems establish a mutually reciprocal agreement where their network traffic pass through each others' networks without charging any fee.
  \item \textbf{Provider relationship} - This relationship occurs when an AS agrees to route the network traffic of another AS based on an agreed upon fee. The AS charging the fee is said to be the provider.
  \item \textbf{Customer relationship} -  This relationship occurs when an AS solicits the help of another AS to route its network traffic after paying some agreed on fee. The AS paying the money is said to be the customer of the other AS.   
  \item \textbf{Sibling relationship} - This relationship usually occurs when an organisation owns multiple autonomous systems. This relationship typically occurs when an Internet Service Provider (ISP) buys another ISP via acquisitions or mergers. In a sibling relationship, an AS can export its routes, customer routes as well as provider and peer routes. 
  \item \textbf{Hybrid relationship} - This BGP business relationships occur when two ASes agree to have a combination of more than business relationship either based on geographical location or IP version.   
\end{itemize}

These relationships are implemented on network routers using BGP path attributes such as the community attribute. The community attribute provides a way of grouping destination that have similar routing decisions applied on them.

As stated earlier, the abstraction proposed in this section hopes to concentrate on the high level BGP business relationship between autonomous systems. Network abstractions are only good if the proposed devices in the experiments are configured and behave as intended by the high level policy specification. A graph is used to express the high level BGP business relationship between autonomous systems. The nodes in the BGP policy intention graph are used to specify the autonomous systems involved in the proposed network. The edges between the nodes (or autonomous systems) are used to indicate the BGP business relationship using the edge label which can be peer, provider, customer or any of the other relationships mentioned above.

The example in figure \ref{fig:hypint} below shows a high level abstraction of a hypothetical relationship between five autonomous systems and their various business relationships.

\begin{figure}[htbp]
\begin{minipage}[b]{0.8\linewidth}
\centering
\includegraphics[width=\linewidth]{phil/hypInternet.eps}
\caption{Abstraction of a hypothetical Internet}
\label{fig:hypint}
\end{minipage}
\end{figure}

The example in figure \ref{fig:hypintlay} below shows the network layout diagram of the hypothetical experiment from the high level BGP routing relationship discussed above. Routers rOrange1 and rOrange2 belong to Orange autonomous system; rTata belongs to Tata autonomous system; rBT belongs to BT autonomous system; rVodafone belongs to Vodafone autonomous system and rVirgin1, rVirgin2 and rVirgin3 belong to Virgin autonomous system. 


\begin{figure}[htbp]
\begin{minipage}[b]{0.85\linewidth}
\centering
\includegraphics[width=\linewidth]{phil/hyointernet.eps}
\caption{Hypothetical Internet Layout}
\label{fig:hypintlay}
\end{minipage}
\end{figure}
                
From the diagram above, all links between routers in the same autonomous system will have internal BGP sessions configured. Links between routers in different autonomous system will however have external BGP sessions configured according to the business relationship from Figure \ref{fig:hypint} above. In order to implement the business relationship between routers in different autonomous systems, we configure community attributes of BGP to achieve low level implementation. The community attribute will be used to tag prefixes belonging to customer, peer and provider routes. The appropriate prefixes depending on the business relationship between the routers are then exported according to the high level policy intentions.


\subsection{Firewall Abstractions}
This section will be giving a brief overview of our proposed high level firewall (or access control) abstractions. A comprehensive review of firewalls, how they work and where they are placed in networks will be discussed in Chapter \ref{chap6:fw}. However, a brief introduction regarding firewalls will be given in the following section.

A firewall is a computer hardware or software that is used to protect individual computers and/or corporate networks from hostile attacks. A firewall achieves this by filtering (or blocking) the passage of undesirable data traffic from crossing into or out of a network. Firewalls block or allow network traffic based on a set of rules established by the organisation in its security policy and configured by the network administrator. The rules define a specific pattern based on a tuple that the firewall detects and an action to be taken. Therefore a firewall rule has the following tuple options:
<action> <protocol> <source IP> <source port> <destination IP> <destination port>
Firewalls try to match network traffic patterns from the first rule to the last as contained in the rule base in a sequential order. Firewalls always enforce the first rule that matches and hence rule ordering in the rule base is very important. The most important and utilised rules should be at the top of the rule base so as to ensure the firewall works efficiently. Firewall rules designed and configured by network administrators may be prone to anomalies that can render parts of the network either unaccessible or open to malicious attacks. There are two categories in which firewall anomalies can be grouped into, these are: intra firewall anomalies and inter firewall anomalies. Intra-firewall anomalies include: shadowing, irrelevance, correlation, generalisation and redundancy. Inter-firewall anomalies include: redundancy, spuriousness, shadowing and correlation.

As stated earlier, the abstraction proposed in this section hopes to concentrate on the high level access control policies using firewalls between network device or group of network devices (or realms) with identical policy intentions. Similar to the abstractions proposed for inter domain routing, graphs will be used to express the high level access control policies between two realms.

The nodes in the firewall policy intention graphs are used to specify realms (a device of group of network devices with identical policies) which transforms on the network layout level as a network firewall enabled device or an IP address in the firewall tuple information during low level firewall commands. For example, universities can choose to group network devices in their networks according to students, staff, campus, laboratory and such. Likewise an organisation can choose to group their network according to staff, visitors, management and departments amongst many others. 


The edges in our firewall policy intention abstraction graphs are used to specify the remaining firewall tuple information between the two realms such as protocol; source and destination ports; and action to be taken on the matched network traffic pattern. Labels of edges are used in specifying whether the action to be taken on the network traffic is either "permit" or "deny". It should be noted that any other phrases used during proposed experiments for abstractions will not work. There are five edge custom properties that have been developed for specifying the other firewall tuple information needed for firewall rules with default values in case a network administrator omits any of them. These edge properties are service (service), protocol (protocol), source port (sPort), destination port (dPort) and an external source/destination IP address or network (dest).

\begin{itemize}
\item The \textbf{protocol} edge property has been developed to give network administrators an avenue of specifying the protocol which can be either TCP, UDP or any other type of protocol value accepted by the firewall vendor product to be used during the experiment. This edge property has a default value of "IP" if a network administrator does not specify any value here.

\item The \textbf{dPort} edge property is used to specify the destination port value for a given firewall tuple as a way of giving network administrators additional granularity during firewall policy abstraction specification. It should be noted that this value when specified along with the service property option discussed above, the pre-configured port number of a service will be overridden with the dPort value. For example, if a network administrator enters www (for web service pre-configured with TCP on ports 80 and 8080) and specifies a \textbf{dPort} value of 52435, ports 80 and 8080 of the pre-configured service value will be overridden with the new \textbf{dPort} value of 52435. This edge property has a default value of "NULL" if a user does not specify any value here.

\item The \textbf{sPort} edge property is used to specify the source port value for a given firewall tuple as a way of giving network administrators additional granularity during firewall policy intention specification. This edge property has a default value of "NULL" if a network adminstrator does not specify any value here.
  
\item The \textbf{service} edge property is used as an abstraction of the protocol and destination port number of a firewall tuple information. This gives the network administrator a way of specifying various network services such as dns (for domain name service), www (for web service using TCP on ports 80 and 8080), torrent (for torrent service using UDP on ports such as). This edge property has a default value of "NULL" if a network administrator does not specify any value here.  
  
\item The \textbf{dest} edge property is used to specify a network device or group of network devices not within the experimental network layout to be deployed. This edge property can either be a source or destination IP address of a network device or group of network devices. This edge property has a default value of "NULL" if a user does not specify any value here. Additional information of how an external source or destination address is specified will be covered in Chapter \ref{chap6:fw}. 
\end{itemize}

The example in Figure \ref{fig:hypfwpol} below shows a high level abstraction of a firewall policies with three realms (group of network devices that have similar policies) and the various access control relationships between them. There are fourteen firewall university network experiment.




\begin{figure}[H]
\begin{minipage}[b]{0.9\linewidth}
\centering
\includegraphics[width=\linewidth]{phil/hypfwpol.eps}
\caption{Abstraction of a hypothetical university network}
\label{fig:hypfwpol}
\end{minipage}
\end{figure}

The full detail of the edge properties used for each firewall rule abstraction is outlined in Table below.

\begin{table}[H]
\centering
\caption{Proposed University Firewall Policy Intention Details}
\label{polint}
\begin{tabular}{@{}|c|c|c|c|c|c|c|@{}}
\toprule
\begin{tabular}[c]{@{}c@{}}Source \\ Realm\end{tabular} & \begin{tabular}[c]{@{}c@{}}Destination \\ Realm\end{tabular} & Action & Protocol & \begin{tabular}[c]{@{}c@{}}Destination \\ Port\end{tabular} & \begin{tabular}[c]{@{}c@{}}Destination \\ Address\end{tabular}                   & \begin{tabular}[c]{@{}c@{}}Source \\ Port\end{tabular} \\ \midrule
any                                                     & any                                                          & permit & ICMP     & NULL                                                        & NULL                                                                             & NULL                                                   \\ \midrule
any                                                     & any                                                          & deny   & TCP      & 80, 8080                                                    & \begin{tabular}[c]{@{}c@{}}d www.gorillavid.in,\\ d www.twitter.com\end{tabular} & NULL                                                   \\ \midrule
management                                              & any                                                          & permit & TCP      & \begin{tabular}[c]{@{}c@{}}40728, \\ 3689\end{tabular}      & NULL                                                                             & NULL                                                   \\ \midrule
management                                              & management                                                   & deny   & ICMP     & NULL                                                        & NULL                                                                             & NULL                                                   \\ \midrule
management                                              & management                                                   & permit & IP       & NULL                                                        & s 1.2.3.4                                                                        & NULL                                                   \\ \midrule
management                                              & research                                                     & deny   & IP       & NULL                                                        & NULL                                                                             & NULL                                                   \\ \midrule
research                                                & management                                                   & permit & TCP      & 54321                                                       & NULL                                                                             & 54321                                                  \\ \midrule
campus                                                  & any                                                          & deny   & TCP      & telnet, ftp                                                 & NULL                                                                             & NULL                                                   \\ \midrule
campus                                                  & campus                                                       & permit & IP       & 80, 8080                                                    & \begin{tabular}[c]{@{}c@{}}d www.gorillavid.in,\\ d www.twitter.com\end{tabular} & NULL                                                   \\ \midrule
any                                                     & campus                                                       & permit & TCP      & ssh                                                         & NULL                                                                             & NULL                                                   \\ \bottomrule
\end{tabular}
\end{table}







\section{Network Layout Level Abstraction}
\label{netlayabs}
The second level of the network management process abstractions showcases how network layouts can be abstracted. It should be noted that the abstractions presented in this section hide details such as IP addresses and proprietary network devices from the network administrator. The abstractions proposed in this section allow network administrators to specify network layouts using graphical editors for proposed experiments. 

The nodes in the network layout graph are used to represent various network devices such as firewalls, servers, routers and others during any proposed experiment. Network devices within the graph can be assigned into various BGP or firewall intention policy realms within any proposed experiment. The policy intention selected at this phase is used in configuring policies that network devices will be using in the proposed experiment.  Node options have been developed to specify which firewall or BGP realm a network device belongs to in a proposed experiment. This node option takes a string value of the policy intention realm's label of the proposed experiment. The value entered must be an exact match of the characters used as the policy intention realm's label.

The edges between the devices in this graph are used to represent the link connecting two network devices. The only set of edge options implemented for our research abstractions on the network layout level has to do with manually specifying IP addresses so as to support the varying degrees of flexibility for network administrators. Edge options called \emph{sIP} and \emph{dIP} are used to specify custom addresses for each link within the proposed network. It should be noted that by default IP addresses are automatically generated for the entire network devices in an experiment. 

This section outlines how to abstract a network topology in two ways - as a chunk of networks and/or slices of networks. 

\subsection{Chunks of Network Topologies}
This section outlines how to abstract a collection of network devices that can be used in multiple instances of an experiment or different experiments by a network administrators. This abstraction has been designed in order to further reduce the time spent by a network administrator when developing an experiment that has multiple instances of the same block or group of network devices during the design process of a proposed experiment. For example, when designing an enterprise firewall network for a university, there might be a need for multiple instances of a laboratory network. A number of departments can all have a laboratory network with the same number of computers; services running on the computers; wireless routers and firewalls as the case maybe. Abstracting this collection of network devices can amongst others: reduce the time needed to  design this block of devices whenever the network administrator encounters such a collection of network devices; reduce the likelihood of mistakes during the design process and ensure the high level policies of this block is the same throughout the proposed network. This motivated us to provide a way of abstracting such blocks for network administrators in an easy and modular way.

This set of abstractions can be used by a network administrator by creating a graph of the proposed block (group of network devices) in exactly the same way as the network layout phase of the system that has been developed for validating our research abstractions. This graph is as described in the Phase II of our system detailed in the previous chapter. Nodes labeled \textbf{ext} are used to indicate where the collection of network devices will be connected to a proposed experiment. It should be noted that there can be more than one connection points for network blocks in any experiment.  The following sections show how this can be achieved.

The collection of network devices, or block, can be placed within the network layout graph during the design process. A sample university network will be used to showcase our proposed abstraction technique in this section. The high level firewall policy intention described in Figure \ref{fig:hypfwpol} above will be used in the example detailed in this section. The network layout graph of the proposed hypothetical university network is depicted in Figure \ref{fig:uninet} below.

% Net Layout
	        \begin{figure}[H]
                \begin{minipage}[b]{1\linewidth}
                \centering
                \includegraphics[width=\linewidth]{phil/fwblocknet.eps}
                \caption{Proposed Hypothetical University Network Layout Diagram}
                \label{fig:uninet}
                \end{minipage}
                \end{figure}

The departments of the proposed university have identical network layouts hence the nodes eDept and cDept have block custom property value of department as they abstractions of such infrastructures. These nodes are abstraction of a collection of network devices used in representing entire departmental networks of the proposed university. Figure \ref{fig:deptblock} below shows the network layout of the collection devices abstracted for the departmental networks.

As indicated in Figure \ref{fig:deptblock} below, all departmental networks will have a set of servers that will be used by staff members of hypothetical university in our example designated as S1, S2 and S3. The network will also have a set of servers for research members of the department designated as R1, R2 and R3 in the same diagram. There is also another abstract network layout representation called Lab for all departmental networks. It should be noted that all the departments in our hypothetical university example have these exact network devices and links as represented in figure \ref{fig:deptblock} below.

The laboratory network depicted in Figure \ref{fig:deptblock} is another abstract representation of a collection of network devices specified as node, Lab in the diagram. The laboratory network of our proposed hypothetical university is also representation of lecture halls where students are taught within departments. The network is composed of student laptops/tablets represented as l1, l2 and l3 in figure \ref{fig:labock} above. The network also has a server which is a machine connected to a projector typically used by the instructor for teaching. The devices www and nfs are used to represent a web server where students can access course materials directly and a storage server for submitting projects respectively. It is expected that all departmental networks in the university have a similar setup for their laboratories.

\begin{figure}
\centering
\begin{minipage}{0.4\textwidth}
\centering
\includegraphics[width=1\textwidth]{phil/deptblock.eps}
\caption{Department Block Network Layout Diagram}
\label{fig:deptblock}
\end{minipage}\hfill
\begin{minipage}{0.4\textwidth}
\centering
\includegraphics[width=1\textwidth]{phil/labock.eps}
\caption{Department Block Network Layout Diagram}
\label{fig:labock}
\end{minipage}
\end{figure}

The diagram in figure \ref{fig:finaluniv} shows the network of how our hypothetical university will look like during low level deployment. As can be seen, both blocks, cDept and eDept, have been added into the final network layout. Likewise the laboratory networks of block Lab nested within the departmental networks has also been added into the final network topology. 

Additional departmental networks can be easily integrated into the existing infrastructure by adding more blocks in figure \ref{fig:uninet} to represent new departments commissioned  by the hypothetical university. The network administrators of the university can easily update agreed upon firewall policy intentions or network infrastructures using abstractions on blocks of departments or laboratories. For example, if the management of the university wants to change the network layout of laboratory networks to include additional network devices or change the firewall policy intention realm of some devices within the infrastructure, they can do so easily without disrupting the university network. The single change will also be replicated across all departmental networks.

% Net Layout
	        \begin{figure}[H]
                \begin{minipage}[b]{1\linewidth}
                \centering
                \includegraphics[width=\linewidth]{phil/finaluniv.eps}
                \caption{Final Rendered Hypothetical University Network Layout Diagram}
                \label{fig:finaluniv}
                \end{minipage}
                \end{figure}

\subsection{Slices of Network Layouts}
The second part of our abstractions in the network layout abstraction level looks at designing network layout slices that will be replicated as the high level network policy intention requires. This enables the replication of identical network layouts except for the allocation of IP addresses for the network devices in the proposed experiment. A good example of where slices of network layouts are used has to do with cyber security competitions abstractions. This is because all participating teams in such competitions need to have identical layouts so as to ensure a level playing field for all participants. A more comprehensive assessment of this network layout abstraction will be carried out in chapter \ref{chap:chapter_7}.




\section{Closing Remarks}
This chapter of the thesis introduced how network concepts can be abstracted so as to make the network management process easier. The chapter introduced template design approaches to abstracting firewall policies; inter domain routing policies and network layout abstraction. This chapter was written so as to show how network processes can be abstracted for the user's benefit. Many other network concepts such as tunnelling, datacenter and other services such as DNS, DHCP can be easily abstracted using techniques detailed in this section of the thesis. The layout diagram in Figure \ref{fig:ilusnet} below details how many other network concepts can be abstracted for network administrators. For example, policies such as all network traffic between the branch networks in the diagram and the HQNet will be encrypted using IPSEC or any other tunnelling protocols can be abstracted on a policy intention level using a between the realms and having a encryption property option.

% Mixed Network Policies
	        \begin{figure}[H]
                \begin{minipage}[b]{1\linewidth}
                \centering
                \includegraphics[width=\linewidth]{phil/ilusdia.eps}
                \caption{Illustrational Topology Showcasing Network Concepts}
                \label{fig:ilusnet}
                \end{minipage}
                \end{figure}

The next chapter of the thesis will be used to introduce and give an overview of the system we developed to evaluate the scalability, flexibility and limitations of the network policy abstraction techniques proposed.