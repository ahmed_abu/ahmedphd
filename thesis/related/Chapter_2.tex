\chapter{Background and Related Work}
\label{chap:chapter_2}


\section{Network Abstraction Design}
Network abstraction is the process of hiding the details or reducing the complexity of a network management process so that the administrator can focus on the high level relationships of the network. By abstracting or hiding details from the network administrator when designing and deploying high-level policies, some of the network anomalies that deal with the configuration process listed in Chapter \ref{chap1:abstraction} can be greatly minimised. Current network abstraction systems allow administrators to concentrate on individual network devices at the topology level through to generation of low level configuration commands without complex network policy intentions implemented on them. Network configuration especially when dealing with large scale networks is known to lead to large outages when small but critical mistakes are made. Critical mistakes of large scale configuration is caused by the long and repetitive nature of such a process. Manual configuration on large scale networks is known to be prone to human errors and consume time. These factors amongst others have made researchers to look at ways of abstracting the network management process in recent years. Some of the benefits of abstracting the network configuration and management include among others: 
\begin{itemize} 	
	\item Automating the network management process will reduce misconfiguration due to human errors to the minimum and ensure end-to-end connectivity of network devices  \cite{Vanbever2008}. 	
		
	\item Automating the network management process will make it easier for operators to deploy networks containing devices from different vendors without learning multiple low level languages\cite{Nguyen2011}.		
	
	\item Automating the network management process will drastically reduce the cost and time it takes to configure networks \cite{Vanbever2008}.
\end{itemize} 
	
Even though abstracting the network management process has many benefits, some researchers however think otherwise for the following reasons amongst others:

\begin{itemize} 	
	\item Automating the network management process can lead to an increased cost of IT operation when the overhead of creating and executing the automation software is high when the task being automated does not need to be repeated often.	
	
	\item By using automation, some valuable information necessary for decision making can be hidden to the network administrator.	
	 	
	\item Lee et al \cite{Lee2008} argue that automating simple and intuitive tasks can hurt network operations in the long run. This is because the network administrator will lose context of what is going on in the network.		
\end{itemize} 
	
Some things that designers of automated configuration tools need to be aware of when creating such software include the following among others:

\begin{itemize} 
	\item Lee et al \cite{Lee2008} argue that automated configuration tools should provide peepholes to the low level details of a network even when the normal requirements by the operators is to hide them.
	
	\item Designers of automated configuration tools should choose the \emph{right} level of abstraction while hiding the unnecessary details \cite{Chen2010}. 	
	 
	\item Designers of automated configuration tools should ensure that their tools support the semantics of various vendor devices and not be bound to a particular implementation \cite{Enck2009}.		
	
	\item Designers of automated configuration tools should ensure their tools meld together different services with varying degrees of interactions and dependencies. 		
\end{itemize} 
	
There are two approaches to designing network abstraction tools - template design approach and language design approach. These two techniques will be discussed in the following section.


%%%%%Template Design Approach%%%%%
\subsection{Template Design}
\label{chap2:template}
The template design approach for designing automated configuration tools uses a bottom-up methodology\cite{Knight}. This approach uses a combination of template snippets and contents from a network information data base(NIDB) so as to generate low level configuration commands. The template snippets which are for various network management processes such as BGP policies contain variables that will be substituted with contents from the NIDB so as to generate configuration files for specific vendor devices. This approach is known to be simple, flexible and extensible \cite{Knight}. Bellovin and Bush \cite{Bellovin2009} created an automated configuration tool using template snippets for various network configuration processes such as BGP peering, packet over SONET(POS) interfaces etc. The tool is used to generate configuration files by substituting some of the variables in the template snippets with data retrieved from a database. 
	
The template snippets support different models and vendor devices including routers and switches. Some of the advantages of the template design approach include amongst others:
(i) there is increased consistency as data items only need to be entered once,		
(ii) new configurations can be pushed out to nodes very quickly,				
(iii) it reduces costs of having to correct errors arising due to manual configurations.
			
Many other developers have used template design approach to create automated configuration tools in the past. Some major disadvantages of using this approach include among others: 
\begin{itemize} 
	\item Efficiently allocating network resources such as IP address blocks and BGP community attributes can become very complex when dealing with large scale networks \cite{Nguyen2011}. 
	
	\item Another major disadvantage of this approach is that it is fundamentally device centric in nature. This is due to the template snippets required for low level configuration commands that will be generated and configured on network devices are for specific vendors.
\end{itemize} 	



%%%%%Language Design Approach%%%%%
\subsection{Language Design} 
The Languages design approach for designing automated configuration tools use a top-down methodology \cite{Knight}. Designers taking this approach to design an automated configuration tool need to write an assembler that can be used by network operators to specify high-level network policies that will be compiled into network configuration files. Examples of how this approach has been used to create automated configuration tools include: 	
\begin{itemize} 
	\item Chen at al \cite{Chen2010} created an automated configuration tool using this approach where they used Mosaic, a variant of Datalog to program automated network operations. Their tool provides network operators a flexible way of programming network-wide management and high-level task schedule using recursive queries. Some advantages of  their automated configuration tool include the following: 
	(i) it is straightforward to write queries that configure routers based on network conditions
	(ii)  the tool can roll back to previous consistent state when a failure or policy violation occurs
	(iii) provides network administrators an easy way of specifying high-level policies 
					
	\item	Voellmy and Hudak \cite{Voellmy2009} have also used language design approach to create their automated configuration tool. The tool uses a compiler created using Haskell language to translate Nettle programs used to generate routing configuration files for XORP routers. This allows network administrators to specify network-wide BGP policies and vendor-specific router details in a uniform language. The tool only supports two routing protocols, BGP and static routes. Nettle only supports XORP routers but the designers intend to support all major router platforms. Some advantages of this automated configuration tool include the following: 
	(i) provides a flexible and safe way to merge multiple routing policies		
	(ii) a single policy specification can be given for a heterogenous collection of routers				
	(iii) it allows operators to specify one cohesive policy for the entire local network routing configuration
\end{itemize}
		
Many other developers have used language design approach to create automated configuration tools in the past. Some major disadvantages of using this approach include among others: 
\begin{itemize} 
	\item The language design approach leads to another language that network administrators need to learn just to use the tool		
	\item When dealing with a compiler that does not support all aspects of the network in use, it  might lead to the network not behaving as intended. 
\end{itemize} 




				
				
\section{BGP Policy Abstraction}
There has been extensive research regarding various aspects of network routing especially on inter domain routing. This research covers a wide range of fields including but not limited to security, traffic engineering, inferring AS relationships and anomaly resolution. However there has not been such extensive research on inter domain routing policy abstraction especially on the business relationship level. The following section summarises some projects that have sought to abstract BGP policy intentions on the business level in the past.

Vanbever et al propose in \cite{vanbever2009} a hierarchical structured model that uses chains and filters to specify various levels of BGP policy abstraction. The proposed model uses two chains of high-level routing filters to express BGP routing policies. The tool implemented was developed in Java and composed of three components:
(i) a data repository containing BGP session descriptions, import and export policy chains;
(ii) a predicate matching engine which relies on regular expressions to represent various attributes and BGP relationships;
(iii) a generation engines that iterates over all BGP sessions and applies import and export chains to generate low-level import and export filters for the sessions.
One of the major drawbacks of the abstraction developed is its lack of a very expressive level of BGP business relationship abstraction. Another major problem with this project is the lack of information with regards to which BGP business relationships are supported if any by this project.

Gottlieb et al proposed in \cite{gottlieb2003} a tool that uses provisioning databases to build configurations of large ISP eBGP sessions. This project only configures customer sessions using a set of technical questionnaires used by the provider to identify unique identifiers on behalf of the customer. The customer-specific information provided in the technical questionnaires are then used by a set of provisioning rules to generate a sequence of Cisco IOS configuration commands for adding the new connection to the network. A database schema is then used for storing and accessing customer-specific data that populates a template for capturing the syntax of the router commands. One of the major drawbacks of this project is its lack of support for other BGP routing business relationships such as transit, peering and siblings. 

Autonetkit \cite{2013g} is a tool that takes high-level network representations and compiles it into a choice of emulated and simulated environments. It uses a graphical editor such as yED to visualise the intended network layout and policy representation within the graph. The network topology graph created is read using NetworkX graph library for python with nodes and edges representing routers and physical connections respectively. Attributes such as AS numbers, link speed or weight for edges can be easily represented just as additional  custom attributes such router type, local preference value amongst others are supported. One of the major limitations of this project is that the level abstraction provided is at the device level or network layout abstraction. Abstract representations of BGP business  relationships at a level higher than the network layout level is not supported.    
 
Cisco VIRL \cite{ciscovirl} is a vendor-specific proprietary simulation platform developed by Cisco. The project uses VM Maestro which is a powerful cross-platform GUI environment that allows a user to drag and drop various network devices in order to represent complex topologies. The versatile tool provided in the project allows a network administrator to either manually configure various network devices or leverage Autonetkit to create bootstrap configurations using official Cisco images for components in the workspace. The project allows network administrators to perform different network measurement activities which includes amongst others: 
(i) ability to see path taken by network using tools such as traceroutes; 
(ii) ability to set the latency, packet loss and jitter on links so as to model complex network environments.
As the automation engine of Cisco VIRL depends on Autonetkit, they share the same limitations. Cisco VIRL just like Autonetkit neither supports a higher level of abstraction on the business relationship level nor a network-wide firewall enabled network such as the one proposed in this project. However, we view Cisco VIRL/Autonetkit and our work as complementary research projects, with each focusing on a different level of abstraction.

\section{Firewall Policy Abstraction}
There has been extensive research about firewalls over the years. The research encompasses configuration management, conflict resolution and visual analysis. The literature on firewall policy abstraction that is scalable, modular, conflict free and easy to use is however very limited. The following section discusses some of the work carried out in field of firewall policy abstraction.

Firewall Builder \cite{fwbuilder} is a GUI based application that can be used to configure firewalls on various platforms such as Cisco (ASA, PIX, router access lists); Linux iptables; HP ProCurve ACL firewalls amongst others. Firewall Builder can be used to configure firewalls using the following five steps: create firewall; define objects; configure policy; compile rules and deploy configuration. This tool does not support automatic generation of IP addresses and hence can be prone to error and tedious consuming. The lack of automatic generation of IP addresses makes Firewall Builder time consuming and prone to error especially when dealing with large enterprise networks. The tool also does not have support for any other network policies such as inter domain routing and hence when dealing with a network that is composed of both firewall and routing policies, it will not be suitable.

The Cisco Adaptive Security Device Manager (Cisco ASDM) \cite{ciscoasdm} is a web-based tool used for configuring, monitoring and troubleshooting Cisco firewall appliances. This tool uses a setup wizard that guides network administrators on how to configure and manage firewall devices. It also provides a built-in packet trace and packet debugging tools for troubleshooting the device. This tool is restricted to configuring only Cisco firewalls and does not support any other platform. It can only configure firewall devices on a per-device basis and not from a network-wide policy abstraction basis. Similar to Firewall Builder, it does not support other network policies such as the ones for inter domain routing and hence is rather limited when dealing with enterprise networks with various network policies.      

Firmato \cite{bartal2004} is a tool used in generating low-level firewall ACLs from high-level policies. The high-level policies are modelled using Entity-Relationship Diagrams that will be used to generate the low-level firewall rules. This tool is highly complex and similar to many existing low-level languages and does not support many vendor hardware devices. This makes it highly unsuitable for network administrators because it is not easy to use. Firmato also does not support NAT which is a very important network policy in the world today. Firmato also cannot express negative ("drop" action) rules. This can make expressing exceptions highly complex as a lot of rules will be needed in order to express negative rules. Similar to the above tools, Firmato does not support any other network policy abstraction and hence is highly limited when it comes to configuring a network with various policies.

Mignis \cite{adao2014} is a semantic based tool used for firewall configurations. Mignis uses a top-down approach that allows the network administrator to specify firewall rules which are then translated into Netfliter firewall configuration commands. The tool takes an abstract firewall rule configuration file which it then uses to generate a series of low-level iptables rules. Mignis supports NAT and the notion of grouping multiple host devices into a security policy group that have similar firewall configurations. One of the major limitations of the Mignis tool is its lack of support for various platforms as it currently only generates iptables rules. Mignis does not support a separated high-level policy and the network policy which makes automatic allocation of IP addresses to the various devices easier to generate. This means the network administrator has to specify the IP addresses of the various devices and hence makes the entire process more time consuming. Mignis also does not support any other network policy abstraction which makes it highly limited and unsuitable for configuring large scale enterprise networks.

 
\section{Cyber Security Competitions Abstraction}
There is a wide range of organisations from academic, government and private institutions that host various kinds of cyber security competitions (CDX) on a yearly basis. There has also been a lot of research and publications on organising CDX events and experiences learnt during such events. However, there are limited proposals on abstracting these events in such a way that will enable organisers to deploy such events with ease and flexibility. The following are the major projects proposed in the past that hope to abstract CDX events so as to make it easy for organisers to design, configure, deploy and manage.

% iCTF
The SecLab Group of University of California in Santa Barbara developed a framework called iCTF \cite{2013c} that can be used to create customisable interactive security competitions. The iCTF architecture is split into two - the game and admin networks. The game network hosts all the virtual machines running the vulnerable software of various participating teams. The virtual machines are VirtualBox appliances which can either be hosted by the various participating teams and connected using a VPN concentrator VM generated by the iCTF. They use VirtualBox in order to be able to leverage its internal networking feature. The admin network is composed of a central database, scorebot and web server. The primary function of the central database is enforcing game rules and keeping track of the competition's state.The scorebot's function is to monitor the status of services running in the game network. The web server is used to host the scoreboard, chat messenger, hints and challenges.


% Tele-Lab
The Tele-Lab \cite{Willems2011} project provides an e-learning system for practical security training. The Tele-Lab server developed for the project consists of two major parts: (i) web-based tutoring system; and (ii) a training environment composed of virtual machines. The tutoring system consists of three kinds of content: information chapters; introduction to security and hacking tool; and practical exercises. The information chapters and introduction to security and hacking tool content of the  tutoring system is used to introduce the user to the theoretical aspects of several networking concepts such as "wireless networks", "reconnaissance" and "malware" amongst others. The practical exercises section are carried out on the training environment (using virtual machines) which can be accessed by users via remote desktop access.

% iTEE
The iTEE project \cite{ernits2015} is an open-source platform that enables creation of interactive cyber security competitions. The iTEE platform developed is composed of three layers: virtualisation layer; virtual lab layer; and virtual learning space layer. (i) The virtualisation layer is used to host the virtual machines created and all the upper layers of the platform depend on this layer. The hypervisor used for this project is based on Oracle VirtualBox Headless run on an Ubuntu Server. (ii) The virtual lab layer is used to provide remote access, control functions for the virtual machines and executing the competitions. The virtual machines are provisioned automatically using a template-based abstraction approach. This leverages on puppet configuration manager, shell and Python scripts to customise the virtual machines and networks. (iii) The virtual learning space layer is used to provide the competition scenario, objectives, automated scoring and automated attacks during the competition.





\section{Closing Remarks}
Template design approach will be employed in abstracting the complex network policies in this thesis. This method was chosen because we believe it will be easier for network administrators to express and deploy experiments in a seamless manner similar to white-board style design currently employed. The network policies chosen in this thesis are complex and designing a low level abstraction language will force network administrators to learn another language which defeats one of the major objectives of this project which is to simplify the network management process. 

The projects described as part of the related work in the above section all fall short of the level of abstraction we have achieved in this thesis. Some of the projects such as Autonetkit and Cisco VIRL only allow network administrators to express networks on a network layout level without complex policies such as inter domain routing relationships and firewall relationships configured on the deployed devices. This limitation means network administrators will have to manually configure such network devices with complex network policy configurations which can be timing consuming, tedious, complacency and likely to introduce anomalies in such networks. Also, to the best of our knowledge there is no proposed inter domain routing abstraction project that uses template design approach that gives network administrator the functionality of expressing network with complex policies such as transit, peer and sibling relationships. This limitation makes it difficult for network administrators to utilise such projects because they would have to manually configure network routers with the other routing relationships that are not supported by the tools being used. This also introduces complacency, time wasting and possibilities of having anomalies in deployed experiments by the network administrators. Lastly, all the network policy abstraction projects currently proposed support only one network policy such as firewall builder for firewalls or propane for inter domain routing. This limitation we posit defeats the purpose of network management process simplification because most networks having two or more network policies working seamlessly together. This limitation forces network administrators to deploy partially configured networks with such tools and having to manually configure the unsupported network policies which can is counterintuitive for network administrators. The focus of the research conducted in this thesis is to develop a set of high level network policy abstractions beyond the network layout level that network administrators can use to design and deploy experiments that have a combination of both firewall and inter domain routing policies. This will be an improvement to current projects and will enable network administrators to design and deploy anomaly free complex networks that have such network policies with ease. 

This chapter of the thesis has introduced the two known techniques for network abstractions: language and template design approaches. The chapter also highlighted and discussed some of the major related research projects conducted on the three network policies focused in this thesis: BGP, firewall relationships and cyber security competitions. The next chapter of this thesis will be exploring the philosophical background of template design abstraction technique which has been adopted for the research conducted in this thesis.
