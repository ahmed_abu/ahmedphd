#!/usr/bin/python

import sys, getopt
import os, optparse
import csv, netaddr
# import pydot
import string
import networkx as nx
import optparse
import logging
import pprint
from lxml import etree
from  netaddr import *
from mako.template import Template
from mako.lookup import TemplateLookup

#parser = optparse.OptionParser()
#
#parser.add_option('-n', '--network',
#                  action='store', dest='network',
#                  help='takes in network level abstraction graph')
#
#parser.add_option('-i', '--intent',
#                  action='store', dest='intent', default= False,
#                  help='takes in intention level abstraction of a cdx graph')
#
#
#(opts, args) = parser.parse_args()

# debugging
# print opts.network
# print opts.cintent

#if opts.network is None or opts.intent is None:
#    print "You must give both intention and network diagram file using -i and -n\n"
#    parser.print_help()
#    exit(-1)



### DECLARATIONS ###
##################################################################################################################
##################################################################################################################
##################################################################################################################
### declarations ###
I = nx.read_graphml(sys.argv[1])
N = nx.read_graphml(sys.argv[2])
# N = N.to_undirected()
loopback = "192.168.0.0"
int_ip = IPAddress("20.0.0.0")
topology = etree.Element("topology", 
						xmlns="http://www.cisco.com/VIRL", 
						# xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
						schemaVersion="0.95" 
# 						xsi:schemaLocation="http://www.cisco.com/VIRL https://raw.github.com/CiscoVIRL/schema/v0.9/virl.xsd"
						)
logging.basicConfig(filename='cdxlog.log',level=logging.DEBUG)
# lookup = TemplateLookup(directories=['./'], module_directory='./')
# 
# test = lookup.get_template("trial.mako")						
lbcount = 0
loopback_subnet = netaddr.IPNetwork("0.0.0.0/32")
int_sub = 0
# fg = pydot.Dot(graph_type='digraph')


### Intention Graph ###
##################################################################################################################
##################################################################################################################
##################################################################################################################

### CAPTURE DEFAULT VALUES FOR INTENTION GRAPH
### Appropriately capture or set default options of various custom properties of intention graph

### Set Default EDGE options ###
for src,dst,data in I.edges_iter(data=True):
    try:
        if not data['service']:
            print ""
    except KeyError:
        data['service'] = 'NULL'

for src,dst,data in I.edges_iter(data=True):
    try:
        if not data['protocol']:
            print ""
    except KeyError:
        data['protocol'] = 'ip'
        
for src,dst,data in I.edges_iter(data=True):
    try:
        if not data['sPort']:
            print ""
    except KeyError:
        data['sPort'] = 'NULL'



### Set Default NODE options ###
for node,data in I.nodes(data=True):
    try:
        if not data['asn']:
            print ""
    except KeyError:
        data['asn'] = 1

for node,data in I.nodes(data=True):
    try:
        if not data['management']:
            print ""
    except KeyError:
        data['management'] = 'true'

for node,data in I.nodes(data=True):
    try:
        if not data['duration']:
            print ""
    except KeyError:
        data['duration'] = 60

for node,data in I.nodes(data=True):
    try:
        if not data['members']:
            print ""
    except KeyError:
        data['members'] = 1
        
        
### Network Layout Graph ###
##################################################################################################################
##################################################################################################################
##################################################################################################################

### CAPTURE DEFAULT VALUES FOR INTENTION GRAPH
### Appropriately capture or set default options of various custom properties of network graph        
              
# Set Default NETWORK GRAPH NODE options
for node,data in N.nodes(data=True):
    try:
        if not data['fwpol']:
            print ""
    except KeyError:
        data['fwpol'] = 'NULL'

for node,data in N.nodes(data=True):
    try:
        if not data['bgppol']:
            print ""
    except KeyError:
        data['bgppol'] = 'NULL'

for node,data in N.nodes(data=True):
    try:
        if not data['cdxpol']:
            print ""
    except KeyError:
        data['cdxpol'] = 'NULL'

for node,data in N.nodes(data=True):
    try:
        if not data['dtype']:
            print ""
    except KeyError:
        data['dtype'] = 'server'
        
       
for node,data in N.nodes(data=True):
    try:
        if not data['network']:
            print ""
    except KeyError:
        data['network'] = 'NULL'


# Set Default NETWORK GRAPH EDGE options
for src,dst,data in N.edges(data=True):
    try:
        if not data['sIP']:
            print ""
    except KeyError:
        data['sIP'] = 'NULL'

for src,dst,data in N.edges(data=True):
    try:
        if not data['dIP']:
            print ""
    except KeyError:
        data['dIP'] = 'NULL'
        
for src,dst,data in N.edges(data=True):
    try:
        if not data['sInt_id']:
            print ""
    except KeyError:
        data['sInt_id'] = 'NULL'

for src,dst,data in N.edges(data=True):
    try:
        if not data['dInt_id']:
            print ""
    except KeyError:
        data['dInt_id'] = 'NULL' 
        
for src,dst,data in N.edges_iter(data=True):
    try:
        if not data['service']:
            print ""
    except KeyError:
        data['service'] = 'NULL'

for src,dst,data in N.edges_iter(data=True):
    try:
        if not data['protocol']:
            print ""
    except KeyError:
        data['protocol'] = 'ip'
        
for src,dst,data in N.edges_iter(data=True):
    try:
        if not data['sPort']:
            print ""
    except KeyError:
        data['sPort'] = 'NULL'       


### FUNCTIONS ###
##################################################################################################################
##################################################################################################################
##################################################################################################################
### FUNCTIONS for assigning AS Number, Device Type, BGP Relationships, Firewall Rules and Addressing 

# Function calls to check the device type and assign a router an AS Number
def assign_bgp(x):
	if x == "NULL":
		asnumber = 1
	else:
		asnumber = asn[x]
	return str(asnumber)

# check device type and set VIRL appropriate version
def check_device(x):
    if x == "server":
        return "server"
    elif x == "router":
        return "IOSv"
    elif x == "fw":
    	return "ASAv"
    elif x == "sw":
    	return "Unmanaged Switch"
    	
# custom firewall configuration - configure access lists for various relationships
def custom_fwconfig(fw):
	if fw == 'cfw':
		rulelist = 'NULL'
		for rule in cdxlist[fw]:
			if rulelist == 'NULL':
				rulelist = rule
			else:
				temp = rulelist
				rulelist = temp+'\n'+rule
		return rulelist
	else:
		rulelist = 'NULL'
		for rule in fwlist[fw]:
			if rulelist == 'NULL':
				rulelist = rule
			else:
				temp = rulelist
				rulelist = temp+'\n'+rule
		lastrule = "access-group vinas-out out interface outside"
		lasttemp = rulelist
		rulelist = lasttemp+'\n'+lastrule+'\n'+"access-group vinas-in in interface outside-1"
		return rulelist

# allocate ip addressing - to be removed
def allocate_ip(x,y):
	int_ip = iplist[x][y]
	return str(int_ip)	
	
# allocate loopback interface addressing appropriately
def allocate_loopback(x,z):
	ip = netaddr.IPAddress(loopback)+z
	return str(ip)	
		
# check neighbors for firewalls and ensure devices not behind a switch or router
def check_neighbors(node, dnode):
	neighs = nx.common_neighbors(N, node, dnode)
	for dev in neighs:
		for cnode,cdata in N.nodes(data=True):
			if dev == cnode: 
				if cdata['dtype'] == 'sw' or cdata['dtype'] == 'router':
					return 'TRUE'
				else:
					return 'FALSE'		

# generate entire firewall configuration codes and bypass autonetkit 
def genfwconfig(u,n):
	count = 1 
	for snode, sdata in N.nodes(data=True):
		if snode == u:
			fconfig = "! ASAv Config generated by NePAS"+"\n"
			fconfig += "!"+"\n"
			fconfig += "hostname "+sdata['label']+"\n"
			fconfig += "username cisco password cisco privilege 15"+"\n"
			fconfig += "enable password cisco"+"\n"
			fconfig += "passwd cisco"+"\n"
			fconfig += "names"+"\n"+"!"+"\n"
			for a,b,c in N.edges(nbunch=[u],data=True):
				if a == u:
					for dnode, ddata in N.nodes(data=True):
						if b == dnode:
							fconfig += "interface GigabitEthernet0/"+str(count)+"\n"
							fconfig += "  description to "+ddata['label']+"\n"
							fconfig += "  duplex full"+"\n"
							if count > 1:
								fconfig += "  nameif nepas-"+ddata['label']+"-"+str(count-1)+"\n"
							else:
								fconfig += "  nameif nepas-"+ddata['label']+"\n"
							fconfig += "  security-level 0"+"\n"
							fconfig += "  no shutdown"+"\n"
							fconfig += "  ip address "+str(allocate_ip(a,b))+" 255.255.255.0"+"\n"
							count = count + 1
			fconfig += "interface Management0/0"+"\n"
			fconfig += " description OOB Management"+"\n"
			fconfig += " duplex full"+"\n"
			fconfig += " management-only"+"\n"
			fconfig += " nameif mgmt"+"\n"
			fconfig += " security-level 100"+"\n"
			fconfig += " no shutdown"+"\n"
			fconfig += " !"+" Configured on launch"+"\n"
			fconfig += " no ip address"+"\n"
			if sdata['label'] == 'cfw':
				fconfig += custom_fwconfig(sdata['label'])+"\n"
			else:
				fconfig += custom_fwconfig(sdata['label'])+"\n"
			fconfig += "!"+"\n"
			fconfig += "same-security-traffic permit inter-interface"+"\n"
			fconfig += "logging enable"+"\n"
			fconfig += "logging asdm informational"+"\n"
			fconfig += "user-identity default-domain LOCAL"+"\n"
			fconfig += "aaa authentication ssh console LOCAL"+"\n"
			fconfig += "http server enable"+"\n"
			fconfig += "http 0.0.0.0 0.0.0.0 mgmt"+"\n"
			fconfig += "ssh 0.0.0.0 0.0.0.0 mgmt"+"\n"
			fconfig += "telnet 0.0.0.0 0.0.0.0 mgmt"+"\n"
			fconfig += "http 0.0.0.0 0.0.0.0 nepas-outside"+"\n"
			fconfig += "ssh 0.0.0.0 0.0.0.0 nepas-outside"+"\n"
			fconfig += "telnet 0.0.0.0 0.0.0.0 nepas-outside"+"\n"
			if count > 2:
				intfaces =  1
				count = count - 1
				while intfaces < count:
					fconfig += "http 0.0.0.0 0.0.0.0 nepas-outside-"+str(intfaces)+"\n"
					fconfig += "ssh 0.0.0.0 0.0.0.0 nepas-outside-"+str(intfaces)+"\n"
					fconfig += "telnet 0.0.0.0 0.0.0.0 nepas-outside-"+str(intfaces)+"\n"
					intfaces = intfaces + 1
			fconfig += "ssh version 2"+"\n"
			fconfig += "crypto key generate rsa modulus 768"+"\n"
			fconfig += "telnet timeout 15"+"\n"
			fconfig += "console timeout 0"+"\n"
			fconfig += "username cisco password cisco privilege 15"+"\n"
			fconfig += "!"+"\n"
			fconfig += "class-map inspection_default"+"\n"
			fconfig += " match default-inspection-traffic"+"\n"
			fconfig += "!"+"\n"
			fconfig += "!"+"\n"
			fconfig += "policy-map type inspect dns preset_dns_map"+"\n"
			fconfig += " parameters"+"\n"
			fconfig += "  message-length maximum client auto"+"\n"
			fconfig += "  message-length maximum 512"+"\n"
			fconfig += "policy-map global_policy"+"\n"
			fconfig += " class inspection_default"+"\n"
			fconfig += "  inspect ip-options"+"\n"
			fconfig += "  inspect netbios"+"\n"
			fconfig += "  inspect rtsp"+"\n"
			fconfig += "  inspect sunrpc"+"\n"
			fconfig += "  inspect tftp"+"\n"
			fconfig += "  inspect xdmcp"+"\n"
			fconfig += "  inspect dns preset_dns_map"+"\n"
			fconfig += "  inspect ftp"+"\n"
			fconfig += "  inspect h323 h225"+"\n"
			fconfig += "  inspect h323 ras"+"\n"
			fconfig += "  inspect rsh"+"\n"
			fconfig += "  inspect esmtp"+"\n"
			fconfig += "  inspect sqlnet"+"\n"
			fconfig += "  inspect sip"+"\n"
			fconfig += "  inspect skinny"+"\n"
			fconfig += "  inspect icmp"+"\n"
			fconfig += "  inspect http"+"\n"
			fconfig += "!"+"\n"
			fconfig += "service-policy global_policy global"+"\n"
			fconfig += "no call-home reporting anonymous"+"\n"
			fconfig += "call-home"+"\n"
			fconfig += " profile CiscoTAC-1"+"\n"
			fconfig += "  no active"+"\n"
			fconfig += "end"+"\n"
	return fconfig

					
### END OF FUNCTIONS ###
##################################################################################################################
##################################################################################################################
##################################################################################################################

cdxtemp = {}
cdxtemp['names'] = []
attServers = []
count = 1
total_attack = 0


for src, dst, data in I.edges(data=True):
	if data['label'] == 'comprehensive' or data['label'] == 'defensive' or data['label'] == 'offensive':
		for node, ndata in I.nodes(data=True):
			if node == src:
				if ndata['label'] not in cdxtemp['names']:
					cdxtemp['names'].append(ndata['label'])
					total_attack = total_attack + int(ndata['members'])
			elif node == dst:
				if ndata['label'] not in cdxtemp['names']:
					cdxtemp['names'].append(ndata['label'])
					total_attack = total_attack + int(ndata['members'])
				
C = nx.Graph()
C.add_node("management", dtype = 'server', label = 'MgtServer', fwpol = 'NULL', bgppol = 'NULL', cdxpol = 'NULL') # for now but change and allow to any
C.add_node("mgrRouter", dtype = 'router', label = 'MgtRouter', bgppol = 'NULL', network = 'NULL', fwpol = 'NULL', cdxpol = 'NULL')
C.add_edge("mgrRouter", "management", sIP = 'NULL', dIP = 'NULL', sInt_id= 'NULL', dInt_id = 'NULL')
C.add_node("cenfw", dtype = 'fw', label = 'cfw', fwpol = 'NULL', bgppol = 'NULL', cdxpol = 'NULL')
C.add_edge("cenfw", "mgrRouter", sIP = 'NULL', dIP = 'NULL', sInt_id= 'NULL', dInt_id = 'NULL')

# for src, dst, data in C.edges(data=True):
# 	print src, dst, data

# This part is used to create the number of attack routers needed to connect the kali linux servers to the cfw of the infrastructure
if total_attack > 14:
	if total_attack < 28:
		C.add_node("arouter", dtype = 'router', label = 'Attack-Router', bgppol = 'NULL', network = 'NULL', fwpol = 'NULL', cdxpol = 'NULL')
		C.add_node("arouter1", dtype = 'router', label = 'Attack-Router-1', bgppol = 'NULL', network = 'NULL', fwpol = 'NULL', cdxpol = 'NULL')
		C.add_edge("cenfw", "arouter", sIP = 'NULL', dIP = 'NULL', sInt_id= 'NULL', dInt_id = 'NULL')
		C.add_edge("cenfw", "arouter1", sIP = 'NULL', dIP = 'NULL', sInt_id= 'NULL', dInt_id = 'NULL')
	elif total_attack < 42:
		C.add_node("arouter", dtype = 'router', label = 'Attack-Router', bgppol = 'NULL', network = 'NULL', fwpol = 'NULL', cdxpol = 'NULL')
		C.add_node("arouter1", dtype = 'router', label = 'Attack-Router-1', bgppol = 'NULL', network = 'NULL', fwpol = 'NULL', cdxpol = 'NULL')
		C.add_node("arouter2", dtype = 'router', label = 'Attack-Router-2', bgppol = 'NULL', network = 'NULL', fwpol = 'NULL', cdxpol = 'NULL')
		C.add_edge("cenfw", "arouter", sIP = 'NULL', dIP = 'NULL', sInt_id= 'NULL', dInt_id = 'NULL')
		C.add_edge("cenfw", "arouter1", sIP = 'NULL', dIP = 'NULL', sInt_id= 'NULL', dInt_id = 'NULL')
		C.add_edge("cenfw", "arouter2", sIP = 'NULL', dIP = 'NULL', sInt_id= 'NULL', dInt_id = 'NULL')
	elif total_attack < 56:
		C.add_node("arouter", dtype = 'router', label = 'Attack-Router', bgppol = 'NULL', network = 'NULL', fwpol = 'NULL', cdxpol = 'NULL')
		C.add_node("arouter1", dtype = 'router', label = 'Attack-Router-1', bgppol = 'NULL', network = 'NULL', fwpol = 'NULL', cdxpol = 'NULL')
		C.add_node("arouter2", dtype = 'router', label = 'Attack-Router-2', bgppol = 'NULL', network = 'NULL', fwpol = 'NULL', cdxpol = 'NULL')
		C.add_node("arouter3", dtype = 'router', label = 'Attack-Router-3', bgppol = 'NULL', network = 'NULL', fwpol = 'NULL', cdxpol = 'NULL')
		C.add_edge("cenfw", "arouter", sIP = 'NULL', dIP = 'NULL', sInt_id= 'NULL', dInt_id = 'NULL')
		C.add_edge("cenfw", "arouter1", sIP = 'NULL', dIP = 'NULL', sInt_id= 'NULL', dInt_id = 'NULL')
		C.add_edge("cenfw", "arouter2", sIP = 'NULL', dIP = 'NULL', sInt_id= 'NULL', dInt_id = 'NULL')
		C.add_edge("cenfw", "arouter3", sIP = 'NULL', dIP = 'NULL', sInt_id= 'NULL', dInt_id = 'NULL')
	else:
		C.add_node("arouter", dtype = 'router', label = 'Attack-Router', bgppol = 'NULL', network = 'NULL', fwpol = 'NULL', cdxpol = 'NULL')
		C.add_node("arouter1", dtype = 'router', label = 'Attack-Router-1', bgppol = 'NULL', network = 'NULL', fwpol = 'NULL', cdxpol = 'NULL')
		C.add_node("arouter2", dtype = 'router', label = 'Attack-Router-2', bgppol = 'NULL', network = 'NULL', fwpol = 'NULL', cdxpol = 'NULL')
		C.add_node("arouter3", dtype = 'router', label = 'Attack-Router-3', bgppol = 'NULL', network = 'NULL', fwpol = 'NULL', cdxpol = 'NULL')
		C.add_node("arouter4", dtype = 'router', label = 'Attack-Router-4', bgppol = 'NULL', network = 'NULL', fwpol = 'NULL', cdxpol = 'NULL')
		C.add_edge("cenfw", "arouter", sIP = 'NULL', dIP = 'NULL', sInt_id= 'NULL', dInt_id = 'NULL')
		C.add_edge("cenfw", "arouter1", sIP = 'NULL', dIP = 'NULL', sInt_id= 'NULL', dInt_id = 'NULL')
		C.add_edge("cenfw", "arouter2", sIP = 'NULL', dIP = 'NULL', sInt_id= 'NULL', dInt_id = 'NULL')
		C.add_edge("cenfw", "arouter3", sIP = 'NULL', dIP = 'NULL', sInt_id= 'NULL', dInt_id = 'NULL')
		C.add_edge("cenfw", "arouter4", sIP = 'NULL', dIP = 'NULL', sInt_id= 'NULL', dInt_id = 'NULL')				
else: 
	C.add_node("arouter", dtype = 'router', label = 'Attack-Router', bgppol = 'NULL', network = 'NULL', fwpol = 'NULL', cdxpol = 'NULL')
	C.add_edge("cenfw", "arouter", sIP = 'NULL', dIP = 'NULL', sInt_id= 'NULL', dInt_id = 'NULL')



# this part is used to create the attack servers and connect them to the attack router
counter = 1
for cnode, cdata in I.nodes(data=True):
	if cdata['label'] != 'any' or cdata['members'] != 0:
		for count in range(1, int(cdata['members']+1)):
			temp = cdata['label']+"-Attack-"+str(count)
			if counter <= 14:
				C.add_node(temp, dtype='server', label = temp, bgppol = 'NULL', fwpol = 'NULL', cdxpol = cdata['label'])
				C.add_edge(temp, "arouter", sIP = 'NULL', dIP = 'NULL', sInt_id = 'NULL', dInt_id = 'NULL')
				attServers.append(temp)
				counter = counter + 1
			elif counter > 14 and counter < 28:
				C.add_node(temp, dtype='server', label = temp, bgppol = 'NULL', fwpol = 'NULL', cdxpol = cdata['label'])
				C.add_edge(temp, "arouter1", sIP = 'NULL', dIP = 'NULL', sInt_id = 'NULL', dInt_id = 'NULL')
				attServers.append(temp)
				counter = counter + 1
			elif counter > 28 and counter < 42:
				C.add_node(temp, dtype='server', label = temp, bgppol = 'NULL', fwpol = 'NULL', cdxpol = cdata['label'])
				C.add_edge(temp, "arouter2", sIP = 'NULL', dIP = 'NULL', sInt_id = 'NULL', dInt_id = 'NULL')
				attServers.append(temp)
				counter = counter + 1
			elif counter > 42 and counter < 56:
				C.add_node(temp, dtype='server', label = temp, bgppol = 'NULL', fwpol = 'NULL', cdxpol = cdata['label'])
				C.add_edge(temp, "arouter3", sIP = 'NULL', dIP = 'NULL', sInt_id = 'NULL', dInt_id = 'NULL')
				attServers.append(temp)
				counter = counter + 1
			elif counter > 56: 
				C.add_node(temp, dtype='server', label = temp, bgppol = 'NULL', fwpol = 'NULL', cdxpol = cdata['label'])
				C.add_edge(temp, "arouter4", sIP = 'NULL', dIP = 'NULL', sInt_id = 'NULL', dInt_id = 'NULL')
				attServers.append(temp)
				counter = counter + 1

# replicating team infrastructure as "teamName-nodeName" - RT teams will not have cyber infrastructure deployed for them as they should not have one
for src, dst, data in N.edges(data=True):
	for snode, sdata in N.nodes(data=True):
		for dnode, ddata in N.nodes(data=True):
			if snode==src and dnode==dst:
				if sdata['label'] == 'cfw':
					for key, value in cdxtemp.iteritems():
						for elem in value:
							name = elem.split('-')
							if name[0] != 'RT':
								dtemp = elem+"-"+ddata['label']
								if ddata['dtype'] == 'router':
									C.add_node("cenfw", dtype=sdata['dtype'], label = sdata['label'], fwpol = 'NULL', bgppol = 'NULL', cdxpol = 'NULL')
									C.add_node(dtemp, dtype=ddata['dtype'], label = dtemp, fwpol = ddata['fwpol'], bgppol = ddata['bgppol'], cdxpol = elem, network = ddata['network'])
									C.add_edge("cenfw", dtemp, sIP = data['sIP'], dIP = data['dIP'], sInt_id= 'NULL', dInt_id = 'NULL')
								else:
									C.add_node("cenfw", dtype=sdata['dtype'], label = sdata['label'], fwpol = 'NULL', bgppol = 'NULL', cdxpol = 'NULL')
									C.add_node(dtemp, dtype=ddata['dtype'], label = dtemp, fwpol = ddata['fwpol'], bgppol = ddata['bgppol'], cdxpol = elem)
									C.add_edge("cenfw", dtemp, sIP = data['sIP'], dIP = data['dIP'], sInt_id= 'NULL', dInt_id = 'NULL')
				elif ddata['label'] == 'cfw':
					for key, value in cdxtemp.iteritems():
						for elem in value:
							name = elem.split('-')
							if name[0] != 'RT':
								stemp = elem+"-"+sdata['label']
								if sdata['dtype'] == 'router':
									C.add_node("cenfw", dtype=ddata['dtype'], label = ddata['label'], fwpol = 'NULL', bgppol = 'NULL', cdxpol = 'NULL')
									C.add_node(stemp, dtype=sdata['dtype'], label = stemp, fwpol = sdata['fwpol'], bgppol = sdata['bgppol'], cdxpol = elem, network = sdata['network'])
									C.add_edge("cenfw", stemp, sIP = data['sIP'], dIP = data['dIP'], sInt_id= 'NULL', dInt_id = 'NULL')
								else:
									C.add_node("cenfw", dtype=ddata['dtype'], label = ddata['label'], fwpol = 'NULL', bgppol = 'NULL', cdxpol = 'NULL')
									C.add_node(stemp, dtype=sdata['dtype'], label = stemp, fwpol = sdata['fwpol'], bgppol = sdata['bgppol'], cdxpol = elem)
									C.add_edge("cenfw", stemp, sIP = data['sIP'], dIP = data['dIP'], sInt_id= 'NULL', dInt_id = 'NULL')
				else:
					for key, value in cdxtemp.iteritems():
						for elem in value:
							name = elem.split('-')
							if name[0] != 'RT':
								stemp = elem+"-"+sdata['label']
								dtemp = elem+"-"+ddata['label']
								if sdata['dtype'] == 'router':
									C.add_node(stemp, dtype=sdata['dtype'], label = stemp, fwpol = sdata['fwpol'], bgppol = sdata['bgppol'], cdxpol = elem, network = sdata['network'])
									C.add_node(dtemp, dtype=ddata['dtype'], label = dtemp, fwpol = ddata['fwpol'], bgppol = ddata['bgppol'], cdxpol = elem)
									C.add_edge(stemp, dtemp, sIP = data['sIP'], dIP = data['dIP'], sInt_id= 'NULL', dInt_id = 'NULL')
								elif ddata['dtype'] == 'router':
									C.add_node(stemp, dtype=sdata['dtype'], label = stemp, fwpol = sdata['fwpol'], bgppol = sdata['bgppol'], cdxpol = elem)
									C.add_node(dtemp, dtype=ddata['dtype'], label = dtemp, fwpol = ddata['fwpol'], bgppol = ddata['bgppol'], cdxpol = elem, network = ddata['network'])
									C.add_edge(stemp, dtemp, sIP = data['sIP'], dIP = data['dIP'], sInt_id= 'NULL', dInt_id = 'NULL')
								elif sdata['dtype'] == 'router' and ddata['dtype'] == 'router':
									C.add_node(stemp, dtype=sdata['dtype'], label = stemp, fwpol = sdata['fwpol'], bgppol = sdata['bgppol'], cdxpol = elem, network = sdata['network'])
									C.add_node(dtemp, dtype=ddata['dtype'], label = dtemp, fwpol = ddata['fwpol'], bgppol = ddata['bgppol'], cdxpol = elem, network = ddata['network'])
									C.add_edge(stemp, dtemp, sIP = data['sIP'], dIP = data['dIP'], sInt_id= 'NULL', dInt_id = 'NULL')
								else:
									C.add_node(stemp, dtype=sdata['dtype'], label = stemp, fwpol = sdata['fwpol'], bgppol = sdata['bgppol'], cdxpol = elem)
									C.add_node(dtemp, dtype=ddata['dtype'], label = dtemp, fwpol = ddata['fwpol'], bgppol = ddata['bgppol'], cdxpol = elem)
									C.add_edge(stemp, dtemp, sIP = data['sIP'], dIP = data['dIP'], sInt_id= 'NULL', dInt_id = 'NULL')


N = C.copy()

	


##################################################################################################################
##################################################################################################################
##################################################################################################################
## Network Dictionaries
iplist = {}
ipadd = {}
complist = {}
list = []
ncount = 0
noint = 0	
i = 1


### DICT 1 - Network Layout Component Details
# network layout graph details...
for node,data in N.nodes(data=True):
	ncount = ncount + 1
	complist[node] = []
	complist[node].append(data['label'])
	complist[node].append(ncount)
	complist[node].append(data['dtype'])
	complist[node].append(data['bgppol'])
	complist[node].append(data['fwpol'])
	complist[node].append(data['cdxpol'])
	complist[node].append(noint)
	complist[node].append(i)

# print complist


#DICT 3 - Assign IP addresses and netmasks to the various interfaces
### IP ADDRESS and SUBNET ALLOCATION ###
for fnode, fdata in N.nodes(data=True):
	iplist[fnode] = {}
	ipadd[fnode] = 'NULL'


for src,dst,data in N.edges(data=True):
	iplist[src][dst] = 'NULL'
	iplist[dst][src] = 'NULL'	

count = 1
#IP Allocation for devices behind switches
for src,dst,data in N.edges(data=True):
	for node, ndata in N.nodes(data=True):
		if node == src and ndata['dtype'] == 'sw':
			for neighs in nx.all_neighbors(N, src):
				if iplist[src][neighs] == 'NULL':
					int_ip = int_ip + 1
					data['sIP'] = str(int_ip)
					iplist[src][neighs] = str(int_ip)
					ipadd[neighs] = str(int_ip)
					complist[neighs].append(str(int_ip))
					count = count + 1
				if iplist[neighs][src] == 'NULL':
					int_ip = int_ip + 1
					data['dIP'] = str(int_ip)
					iplist[neighs][src] = str(int_ip)
					ipadd[neighs] = str(int_ip)
					complist[neighs].append(str(int_ip))
					count = count + 1
			temp = 255 - (count - 2)
			int_ip = int_ip + temp
			count = 1 

count = 1
#IP Allocation for other topology devices	
for src,dst,data in N.edges(data=True):
	if iplist[src][dst] == 'NULL':
		int_ip = int_ip + 1
		data['sIP'] = str(int_ip)
		ipadd[src] = str(int_ip) 
		iplist[src][dst] = str(int_ip)
		count = count + 1
		for snode, sdata in N.nodes(data=True):
			if snode == src and sdata['dtype'] == "server":
				complist[snode].append(str(int_ip))
		int_ip = int_ip + 1
		iplist[dst][src] = str(int_ip)
		data['dIP'] = str(int_ip)
		ipadd[dst] = str(int_ip)
		count = count + 1
		for dnode, ddata in N.nodes(data=True):
			if dnode == dst and ddata['dtype'] == "server":
				complist[dnode].append(str(int_ip))
	if count != 1:
		temp = 255 - (count - 2) 	
		int_ip = int_ip + temp
		count = 1 

# for src, dst, sdata in N.edges(data=True):
# 	print src, dst, sdata['sIP'], sdata['dIP'] 
##################################################################################################################
##################################################################################################################
##################################################################################################################
cdxlist = {}
cdxlist["cfw"] = [] 

### TODO: CDX Infrastructure Access Rules for the following:-

###       allow management server have access to all devices - DONE
fwrule = "access-list nepas-mgt-out extended permit ip host "+ipadd["management"]+" any"
cdxlist["cfw"].append(fwrule)

### TODO: check whether the team management is true and allow all team members access to mgt server - DONE
for node, data in I.nodes(data=True):
	if data['management'] == 'true':
		for nnode, ndata in N.nodes(data=True):
			if ndata['dtype'] != 'sw' and ndata['dtype'] != 'router' and ndata['dtype'] != 'fw':
				name = ndata['label'].split('-')
				if name[0] == data['label'] and name[1]!='Attack':
					fwrule = "access-list nepas-mgt-in extended permit ip host "+complist[nnode][8]+" host "+ipadd["management"]
					cdxlist["cfw"].append(fwrule)

# attack servers should have access to the vulnerable server of their opponents as indicated in the policy intention graph (DONE)
for snode, sdata in N.nodes(data=True):
	if snode in attServers: 
		for dnode, ddata in N.nodes(data=True):
			if snode != dnode and dnode not in attServers:
				if ddata['dtype'] != 'sw' and ddata['cdxpol'] != 'NULL' and ddata['dtype'] != 'router' and ddata['dtype'] != 'fw': 
					if sdata['cdxpol'] == ddata['cdxpol']:
						neighs = check_neighbors(snode, dnode)
#  						print neighs
						if neighs == 'TRUE':
							logging.warning('CDX devices %s and %s behind switch or router', sdata['label'], ddata['label'])
						else:###       attack servers should have access to all devices in their team so as to be able to fix vulnerabilities 
							fwrule = "access-list attack-own-out extended permit ip host "+complist[snode][8]+" host "+complist[dnode][8]
							cdxlist["cfw"].append(fwrule)		
					else: ###          attack servers should have appropriate access to other teams' vuln servers (*****)
						for sinode, sidata in I.nodes(data=True):
							if sidata['label'] == sdata['cdxpol']:
								for dinode, didata in I.nodes(data=True):
									if didata['label'] == ddata['cdxpol']:
										for src, dst, idata in I.edges(data=True):
											if sinode == src and dinode == dst or sinode == dst and dinode == src:
												if idata['label'] == 'comprehensive':
													print sidata['label'], didata['label']
													fwrule = "access-list attack-opp-out extended permit ip host "+complist[snode][8]+" host "+complist[dnode][8]
													cdxlist["cfw"].append(fwrule)
												elif idata['label'] == 'offensive':
													fwrule = "access-list attack-opp-out extended permit ip host "+complist[snode][8]+" host "+complist[dnode][8]
													cdxlist["cfw"].append(fwrule)
												elif idata['label'] == 'defensive':
													fwrule = "access-list attack-opp-out extended permit ip host "+complist[snode][8]+" host "+complist[dnode][8]
													cdxlist["cfw"].append(fwrule)

# print cdxlist
##################################################################################################################
##################################################################################################################
##################################################################################################################
### BUILIDING VIRL VIA LXML	FILE ###		
##################################################################################################################
##################################################################################################################
##################################################################################################################
# for node, data in N.nodes(data=True):
# 	print node, data

nx.write_graphml(N, "test.graphml", prettyprint=True)

temp = 1
### TASK 1 - Nodes and Interfaces
for u,n in N.nodes(data=True): 
	if n['dtype'] == "server":
		node = etree.SubElement(topology, "node",
								name = n['label'],
								type = "SIMPLE",
								subtype = check_device(n['dtype'])
		)
		extensions = etree.SubElement(node, "extensions")
		entry = etree.SubElement(extensions, "entry",
									 key = "AutoNetkit.ASN",
									 type = "Integer"
									 )
		entry.text = assign_bgp(n['bgppol'])
		for a,b,c in N.edges(nbunch=[u],data=True):
			if a == u :
				complist[a][7] = complist[a][7] + 1
				complist[a][6] = complist[a][6] + 1
				if c['sInt_id'] == 'NULL':
					c['sInt_id'] = complist[a][6]
				else:
					c['dInt_id'] = complist[a][6]
				interface = etree.SubElement(node, "interface", id = str(complist[a][6]), name="eth"+str(complist[a][7]), ipv4 = allocate_ip(a,b), netPrefixLenV4="24")
			elif b == u :
				print "yes"
				complist[a][6] = complist[a][6] + 1
				interface = etree.SubElement(node, "interface", id = str(complist[a][6]), name="eth"+str(complist[a][7]), ipv4 = allocate_ip(a,b), netPrefixLenV4="24")	
	elif n['dtype'] == "fw":
		node = etree.SubElement(topology, "node",
								name = n['label'],
								type = "SIMPLE",
								subtype = check_device(n['dtype'])
		)
		extensions = etree.SubElement(node, "extensions")
		entry = etree.SubElement(extensions, "entry",
									 key = "AutoNetkit.custom_config_global",
									 type = "string"
									 )
# 		entry.text = custom_fwconfig(n['label'])
		entry = etree.SubElement(extensions, "entry",
									 key = "AutoNetkit.ASN",
									 type = "Integer"
									 )
		entry.text = genfwconfig(u,n)
		entry = etree.SubElement(extensions, "entry",
									 key = "AutoNetkit.ASN",
									 type = "Integer"
									 )
		entry.text = assign_bgp(n['bgppol'])
		for a,b,c in N.edges(nbunch=[u],data=True):
			if a == u :
				complist[a][7] = complist[a][7] + 1
				complist[a][6] = complist[a][6] + 1
				if c['sInt_id'] == 'NULL':
					c['sInt_id'] = complist[a][6]
				else:
					c['dInt_id'] = complist[a][6]
				interface = etree.SubElement(node, "interface", id = str(complist[a][6]), name="GigabitEthernet0/"+str(complist[a][6]), ipv4 = allocate_ip(a,b), netPrefixLenV4="24")
			elif b == u :
				print "yes"
				complist[a][6] = complist[a][6] + 1
				interface = etree.SubElement(node, "interface", id = str(complist[a][6]), name="GigabitEthernet0/"+str(complist[a][6]), ipv4 = allocate_ip(a,b), netPrefixLenV4="24")	
	elif n['dtype'] == "sw":
		node = etree.SubElement(topology, "node",
								name = n['label'],
								type = "SIMPLE",
								subtype = check_device(n['dtype'])
		)
		for a,b,c in N.edges(nbunch=[u],data=True):
			if a == u :
				complist[a][7] = complist[a][7] + 1
				complist[a][6] = complist[a][6] + 1
				if c['sInt_id'] == 'NULL':
					c['sInt_id'] = complist[a][6]
				else:
					c['dInt_id'] = complist[a][6]
				interface = etree.SubElement(node, "interface", id = str(complist[a][6]), name = "link"+str(complist[a][7]))
			elif b == u :
				print "yes"
				complist[a][6] = complist[a][6] + 1
				interface = etree.SubElement(node, "interface", id = str(complist[a][6]), name = "link"+str(complist[a][7])) 
# 	else:
# 		lbcount = lbcount + 1
# 		node = etree.SubElement(topology, "node",
# 								name = n['label'],
# 								type = "SIMPLE",
# 								print ("yes")
# 								subtype = check_device(n['dtype'])
# 		)
# 		if n['bgppol'] != 'NULL':
# 			extensions = etree.SubElement(node, "extensions")
# 			entry = etree.SubElement(extensions, "entry",
# 									 key = "AutoNetkit.ASN",
# 									 type = "Integer"
# 									 )
# 			entry.text = assign_bgp(n['bgppol'])
# 			entry = etree.SubElement(extensions, "entry",
# 										 key = "AutoNetkit.custom_config_global",
# 										 type = "string"
# 										 )
# 			entry.text = bgp_globalconfig(n['label'])
# 			entry = etree.SubElement(extensions, "entry",
# 									 key = "AutoNetkit.custom_config_bgp",
# 									 type = "string"
# 									 )
# 			entry.text = custom_bgpconfig(n['network'], n['label'])
# 		for a,b,c in N.edges(nbunch=[u],data=True):
# 			if a == u:
# 				complist[a][7] = complist[a][7] + 1
# 				complist[a][6] = complist[a][6] + 1
# 				if c['sInt_id'] == 'NULL':
# 					c['sInt_id'] = complist[a][6]
# 				else:
# 					c['dInt_id'] = complist[a][6]
# 				interface = etree.SubElement(node, "interface", id = str(complist[a][6]), name="GigabitEthernet0/"+str(complist[a][7]), ipv4 = allocate_ip(a,b), netPrefixLenV4="24")
# 			elif b == u : 
# 				print "yes"
# 				complist[a][6] = complist[a][6] + 1
# 				interface = etree.SubElement(node, "interface", id = str(complist[a][6]), name="GigabitEthernet0/"+str(complist[a][7]), ipv4 = allocate_ip(a,b), netPrefixLenV4="24")			     	
##################################################################################################################
### TASK 2 - Connections between devices in XML
annotations = etree.SubElement(topology, "annotations")
for src,dst,data in N.edges(data=True):
	if data['sInt_id'] == 'NULL' or data['dInt_id'] == 'NULL':
		logging.warning('One of your interface ids is NULL')
	else:
		connection = etree.SubElement(topology, "connection", 
										dst = "/virl:topology/virl:node["+str(complist[src][1])+"]/virl:interface["+str(data['sInt_id'])+"]", 
								 		src = "/virl:topology/virl:node["+str(complist[dst][1])+"]/virl:interface["+str(data['dInt_id'])+"]"
										)

 
               
print(etree.tostring(topology, pretty_print=True))