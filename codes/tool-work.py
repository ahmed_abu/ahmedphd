#!/usr/bin/python

import sys, getopt
import os, optparse
import csv, netaddr
import string
import networkx as nx
import optparse
import logging
import pprint
from lxml import etree
from  netaddr import *
from mako.template import Template
from mako.lookup import TemplateLookup

#parser = optparse.OptionParser()
#
#parser.add_option('-n', '--network',
#                  action='store', dest='network',
#                  help='takes in network level abstraction graph')
#
#parser.add_option('-i', '--intent',
#                  action='store', dest='intent', default= False,
#                  help='takes in intention level abstraction of a cdx graph')
#
#
#(opts, args) = parser.parse_args()

# debugging
# print opts.network
# print opts.cintent

#if opts.network is None or opts.intent is None:
#    print "You must give both intention and network diagram file using -i and -n\n"
#    parser.print_help()
#    exit(-1)



### DECLARATIONS ###
##################################################################################################################
##################################################################################################################
##################################################################################################################
### declarations ###
I = nx.read_graphml(sys.argv[1])
N = nx.read_graphml(sys.argv[2])
# N = N.to_undirected()
loopback = "192.168.0.0"
int_ip = IPAddress("20.0.0.0")
topology = etree.Element("topology", 
						xmlns="http://www.cisco.com/VIRL", 
						# xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
						schemaVersion="0.95" 
# 						xsi:schemaLocation="http://www.cisco.com/VIRL https://raw.github.com/CiscoVIRL/schema/v0.9/virl.xsd"
						)
logging.basicConfig(filename='vinas.log',level=logging.DEBUG)
# lookup = TemplateLookup(directories=['./'], module_directory='./')
# 
# test = lookup.get_template("trial.mako")						
lbcount = 0
loopback_subnet = netaddr.IPNetwork("0.0.0.0/32")
int_sub = 0


### Intention Graph ###
##################################################################################################################
##################################################################################################################
##################################################################################################################

### CAPTURE DEFAULT VALUES FOR INTENTION GRAPH
### Appropriately capture or set default options of various custom properties of intention graph

### Set Default EDGE options ###
for src,dst,data in I.edges_iter(data=True):
    try:
        if not data['service']:
            print ""
    except KeyError:
        data['service'] = 'NULL'

for src,dst,data in I.edges_iter(data=True):
    try:
        if not data['protocol']:
            print ""
    except KeyError:
        data['protocol'] = 'ip'
        
for src,dst,data in I.edges_iter(data=True):
    try:
        if not data['sPort']:
            print ""
    except KeyError:
        data['sPort'] = 'NULL'



### Set Default NODE options ###
for node,data in I.nodes(data=True):
    try:
        if not data['asn']:
            print ""
    except KeyError:
        data['asn'] = 1

for node,data in I.nodes(data=True):
    try:
        if not data['management']:
            print ""
    except KeyError:
        data['management'] = 'true'

for node,data in I.nodes(data=True):
    try:
        if not data['duration']:
            print ""
    except KeyError:
        data['duration'] = 60

for node,data in I.nodes(data=True):
    try:
        if not data['members']:
            print ""
    except KeyError:
        data['members'] = 1
        
        
### Network Layout Graph ###
##################################################################################################################
##################################################################################################################
##################################################################################################################

### CAPTURE DEFAULT VALUES FOR INTENTION GRAPH
### Appropriately capture or set default options of various custom properties of network graph        
              
# Set Default NETWORK GRAPH NODE options
for node,data in N.nodes(data=True):
    try:
        if not data['fwpol']:
            print ""
    except KeyError:
        data['fwpol'] = 'NULL'

for node,data in N.nodes(data=True):
    try:
        if not data['bgppol']:
            print ""
    except KeyError:
        data['bgppol'] = 'NULL'

for node,data in N.nodes(data=True):
    try:
        if not data['cdxpol']:
            print ""
    except KeyError:
        data['cdxpol'] = 'NULL'

for node,data in N.nodes(data=True):
    try:
        if not data['dtype']:
            print ""
    except KeyError:
        data['dtype'] = 'server'
        
for node,data in N.nodes(data=True):
    try:
        if not data['nat']:
            print ""
    except KeyError:
        data['nat'] = 'false'
        
for node,data in N.nodes(data=True):
    try:
        if not data['network']:
            print ""
    except KeyError:
        data['network'] = 'NULL'


# Set Default NETWORK GRAPH EDGE options
for src,dst,data in N.edges(data=True):
    try:
        if not data['sIP']:
            print ""
    except KeyError:
        data['sIP'] = 'NULL'

for src,dst,data in N.edges(data=True):
    try:
        if not data['dIP']:
            print ""
    except KeyError:
        data['dIP'] = 'NULL'
        
for src,dst,data in N.edges(data=True):
    try:
        if not data['sInt_id']:
            print ""
    except KeyError:
        data['sInt_id'] = 'NULL'

for src,dst,data in N.edges(data=True):
    try:
        if not data['dInt_id']:
            print ""
    except KeyError:
        data['dInt_id'] = 'NULL' 
        
for src,dst,data in N.edges_iter(data=True):
    try:
        if not data['service']:
            print ""
    except KeyError:
        data['service'] = 'NULL'

for src,dst,data in N.edges_iter(data=True):
    try:
        if not data['protocol']:
            print ""
    except KeyError:
        data['protocol'] = 'ip'
        
for src,dst,data in N.edges_iter(data=True):
    try:
        if not data['sPort']:
            print ""
    except KeyError:
        data['sPort'] = 'NULL'       



### FUNCTIONS ###
##################################################################################################################
##################################################################################################################
##################################################################################################################
### FUNCTIONS for assigning AS Number, Device Type, BGP Relationships, Firewall Rules and Addressing 

# Function calls to check the device type and assign a router an AS Number
def assign_bgp(x):
	asnumber = asn[x]
	return str(asnumber)


# check device type and set VIRL appropriate version
def check_device(x):
    if x == "server":
        return "server"
    elif x == "router":
        return "IOSv"
    elif x == "fw":
    	return "ASAv"
    elif x == "sw":
    	return "Unmanaged Switch"


# custom bgp configuration - add router networks and configure access lists for various relationships
def custom_bgpconfig(networks, name):
	cbgp = 'NULL'
	routes = networks.split(',')
	if networks != 'NULL':
		for route in routes:
			if cbgp == 'NULL':
				cbgp = "network "+route
			else:
				temp = cbgp
				cbgp = temp+"\n"+"network "+route
	else: 
		cbgp = " "
	# bgprules = bgplist[name].split(' ')
	for rule in bgplist[name]:
		temp = cbgp
		cbgp = temp+"\n"+rule
	return cbgp


# configure 
def bgp_globalconfig(name):
	access = 'NULL'
	for acl in  bgpaccess[name]:
		if access == 'NULL':
			access = acl
		else:
			temp = access
			access = temp+"\n"+acl
	final = access+"\n"+"access-list 2 deny 0.0.0.0 255.255.255.255" 
	return final


# custom firewall configuration - configure access lists for various relationships
def custom_fwconfig(fw):
	rulelist = 'NULL'
	for rule in fwlist[fw]:
		if rulelist == 'NULL':
			rulelist = rule
		else:
			temp = rulelist
			rulelist = temp+'\n'+rule
	lastrule = "access-group vinas-out out interface outside"
	lasttemp = rulelist
	rulelist = lasttemp+'\n'+lastrule+'\n'+"access-group vinas-in in interface outside-1"
	return rulelist


# allocate ip addressing - to be removed
def allocate_ip(x,y):
	int_ip = iplist[x][y]
	return str(int_ip)
	
	
# allocate loopback interface addressing appropriately
def allocate_loopback(x,z):
	ip = netaddr.IPAddress(loopback)+z
	return str(ip)	
	
	
# configure BGP rules for provider and sibling relationships
def export_allroutes(ip, asn):
	rule = "neighbor "+str(ip)+" distribute-list 1 out"
	access = "access-list 1 permit 0.0.0.0 255.255.255.255" 	
	return rule, access
	
	
# configure BGP rules for customer and peer relationships
def export_custroutes(sip, dip, asn, networks):
	netaccess = 'NULL'
	rule = "neighbor "+str(dip)+" distribute-list 2 out"
	if networks == 'NULL':
		netaccess = "access-list 2 permit "+str(sip)+" 255.255.255.252"
	else:
		nets = networks.split(',')
		for net in nets:
			access = "access-list 2 permit "+net+" 255.255.255.252"
			if netaccess == 'NULL':
				netaccess = access
			else:
				temp = netaccess
				netaccess = temp+'\n'+access	
	return rule, netaccess


# check neighbors for firewalls and ensure devices not behind a switch or router
def check_neighbors(node, dnode):
	neighs = nx.common_neighbors(N, node, dnode)
	for dev in neighs:
		for cnode,cdata in N.nodes(data=True):
			if dev == cnode: 
				if cdata['dtype'] == 'sw' or cdata['dtype'] == 'router':
					return 'TRUE'
				else:
					return 'FALSE'		
					
### END OF FUNCTIONS ###
##################################################################################################################
##################################################################################################################
##################################################################################################################
              
if str(sys.argv[3]) == 'cdx':
	cdxtemp = {}
	cdxtemp['names'] = []
	attServers = []
	count = 1
	total_attack = 0

	
	for src, dst, data in I.edges(data=True):
		if data['label'] == 'comprehensive' or data['label'] == 'defensive' or data['label'] == 'offensive':
			for node, ndata in I.nodes(data=True):
				if node == src:
					if ndata['label'] not in cdxtemp['names']:
						cdxtemp['names'].append(ndata['label'])
						total_attack = total_attack + int(ndata['members'])
				elif node == dst:
					if ndata['label'] not in cdxtemp['names']:
						cdxtemp['names'].append(ndata['label'])
						total_attack = total_attack + int(ndata['members'])
					
	C = nx.Graph()
	C.add_node("arouter", dtype = 'brouter', label = 'Attack-Router', fwpol = 'NULL', bgppol = 'NULL', network = 'NULL', cdxpol = 'NULL')
	C.add_node("management", dtype = 'server', label = 'MgtServer', fwpol = 'NULL', bgppol = 'NULL', cdxpol = 'NULL') # for now but change and allow to any

	for node, data in N.nodes(data=True):
		if data['label'] == 'cfw' and data['dtype'] == 'fw':
			C.add_edge(node, "management", sIP = 'NULL', dIP = 'NULL', sInt_id= 'NULL', dInt_id = 'NULL')
			C.add_edge(node, "arouter", sIP = 'NULL', dIP = 'NULL', sInt_id= 'NULL', dInt_id = 'NULL')

	for cnode, cdata in I.nodes(data=True):
		if cdata['label'] != 'any':
			for count in range(1, int(cdata['members']+1)):
				temp = cdata['label']+"-Attack-"+str(count)
				C.add_node(temp, dtype='server', label = temp, cdxpol = cdata['label'], bgppol = 'NULL', fwpol = 'NULL')
				C.add_edge(temp, "arouter", sIP = 'NULL', dIP = 'NULL', sInt_id = 'NULL', dInt_id = 'NULL')
				attServers.append(temp)


	for src, dst, data in N.edges(data=True):
		for snode, sdata in N.nodes(data=True):
			for dnode, ddata in N.nodes(data=True):
				if snode==src and dnode==dst:
					if sdata['label'] == 'cfw':
						for key, value in cdxtemp.iteritems():
							for elem in value:
								dtemp = elem+"-"+ddata['label']
								if ddata['dtype'] == 'router':
									C.add_node(snode, dtype=sdata['dtype'], label = sdata['label'], fwpol = 'NULL', bgppol = 'NULL', cdxpol = 'NULL')
									C.add_node(dtemp, dtype=ddata['dtype'], label = dtemp, fwpol = ddata['fwpol'], bgppol = ddata['bgppol'], cdxpol = elem, network = ddata['network'])
									C.add_edge(snode, dtemp, sIP = data['sIP'], dIP = data['dIP'], sInt_id= 'NULL', dInt_id = 'NULL')
								else:
									C.add_node(snode, dtype=sdata['dtype'], label = sdata['label'], fwpol = 'NULL', bgppol = 'NULL', cdxpol = 'NULL')
									C.add_node(dtemp, dtype=ddata['dtype'], label = dtemp, fwpol = ddata['fwpol'], bgppol = ddata['bgppol'], cdxpol = elem)
									C.add_edge(snode, dtemp, sIP = data['sIP'], dIP = data['dIP'], sInt_id= 'NULL', dInt_id = 'NULL')
					elif ddata['label'] == 'cfw':
						for key, value in cdxtemp.iteritems():
							for elem in value:
								stemp = elem+"-"+sdata['label']
								if sdata['dtype'] == 'router':
									C.add_node(dnode, dtype=ddata['dtype'], label = ddata['label'], fwpol = 'NULL', bgppol = 'NULL', cdxpol = 'NULL')
									C.add_node(stemp, dtype=sdata['dtype'], label = stemp, fwpol = sdata['fwpol'], bgppol = sdata['bgppol'], cdxpol = elem, network = sdata['network'])
									C.add_edge(dnode, stemp, sIP = data['sIP'], dIP = data['dIP'], sInt_id= 'NULL', dInt_id = 'NULL')
								else:
									C.add_node(dnode, dtype=ddata['dtype'], label = ddata['label'], fwpol = 'NULL', bgppol = 'NULL', cdxpol = 'NULL')
									C.add_node(stemp, dtype=sdata['dtype'], label = stemp, fwpol = sdata['fwpol'], bgppol = sdata['bgppol'], cdxpol = elem)
									C.add_edge(dnode, stemp, sIP = data['sIP'], dIP = data['dIP'], sInt_id= 'NULL', dInt_id = 'NULL')
					else:
						for key, value in cdxtemp.iteritems():
							for elem in value:
								stemp = elem+"-"+sdata['label']
								dtemp = elem+"-"+ddata['label']
								if sdata['dtype'] == 'router':
									C.add_node(stemp, dtype=sdata['dtype'], label = stemp, fwpol = sdata['fwpol'], bgppol = sdata['bgppol'], cdxpol = elem, network = sdata['network'])
									C.add_node(dtemp, dtype=ddata['dtype'], label = dtemp, fwpol = ddata['fwpol'], bgppol = ddata['bgppol'], cdxpol = elem)
									C.add_edge(stemp, dtemp, sIP = data['sIP'], dIP = data['dIP'], sInt_id= 'NULL', dInt_id = 'NULL')
								elif ddata['dtype'] == 'router':
									C.add_node(stemp, dtype=sdata['dtype'], label = stemp, fwpol = sdata['fwpol'], bgppol = sdata['bgppol'], cdxpol = elem)
									C.add_node(dtemp, dtype=ddata['dtype'], label = dtemp, fwpol = ddata['fwpol'], bgppol = ddata['bgppol'], cdxpol = elem, network = ddata['network'])
									C.add_edge(stemp, dtemp, sIP = data['sIP'], dIP = data['dIP'], sInt_id= 'NULL', dInt_id = 'NULL')
								elif sdata['dtype'] == 'router' and ddata['dtype'] == 'router':
									C.add_node(stemp, dtype=sdata['dtype'], label = stemp, fwpol = sdata['fwpol'], bgppol = sdata['bgppol'], cdxpol = elem, network = sdata['network'])
									C.add_node(dtemp, dtype=ddata['dtype'], label = dtemp, fwpol = ddata['fwpol'], bgppol = ddata['bgppol'], cdxpol = elem, network = ddata['network'])
									C.add_edge(stemp, dtemp, sIP = data['sIP'], dIP = data['dIP'], sInt_id= 'NULL', dInt_id = 'NULL')
								else:
									C.add_node(stemp, dtype=sdata['dtype'], label = stemp, fwpol = sdata['fwpol'], bgppol = sdata['bgppol'], cdxpol = elem)
									C.add_node(dtemp, dtype=ddata['dtype'], label = dtemp, fwpol = ddata['fwpol'], bgppol = ddata['bgppol'], cdxpol = elem)
									C.add_edge(stemp, dtemp, sIP = data['sIP'], dIP = data['dIP'], sInt_id= 'NULL', dInt_id = 'NULL')


	N = C.copy()
	##################################################################################################################
	##################################################################################################################
	##################################################################################################################
	## Network Dictionaries
	iplist = {}
	ipadd = {}
	complist = {}
	list = []
	ncount = 0
	noint = 0	
	i = 1

	
	### DICT 1 - Network Layout Component Details
	# network layout graph details...
	for node,data in N.nodes(data=True):
		ncount = ncount + 1
		complist[node] = []
		complist[node].append(data['label'])
		complist[node].append(ncount)
		complist[node].append(data['dtype'])
		complist[node].append(data['bgppol'])
		complist[node].append(data['fwpol'])
		complist[node].append('Meant to be CDXPOL')
		complist[node].append(noint)
		complist[node].append(i)
	# print complist



	#DICT 3 - Assign IP addresses and netmasks to the various interfaces
	### IP ADDRESS and SUBNET ALLOCATION ###
	for fnode, fdata in N.nodes(data=True):
		iplist[fnode] = {}
		ipadd[fnode] = 'NULL'

	
	for src,dst,data in N.edges(data=True):
		iplist[src][dst] = 'NULL'
		iplist[dst][src] = 'NULL'	

	count = 1
	#IP Allocation for devices behind switches
	for src,dst,data in N.edges(data=True):
		for node, ndata in N.nodes(data=True):
			if node == src and ndata['dtype'] == 'sw':
				for neighs in nx.all_neighbors(N, src):
					if iplist[src][neighs] == 'NULL':
						int_ip = int_ip + 1
						data['sIP'] = str(int_ip)
						iplist[src][neighs] = str(int_ip)
						ipadd[neighs] = str(int_ip)
						complist[neighs].append(str(int_ip))
						count = count + 1
					if iplist[neighs][src] == 'NULL':
						int_ip = int_ip + 1
						data['dIP'] = str(int_ip)
						iplist[neighs][src] = str(int_ip)
						ipadd[neighs] = str(int_ip)
						complist[neighs].append(str(int_ip))
						count = count + 1
				temp = 255 - (count - 2)
				int_ip = int_ip + temp
				count = 1 

	count = 1
	#IP Allocation for other topology devices	
	for src,dst,data in N.edges(data=True):
		if iplist[src][dst] == 'NULL':
			int_ip = int_ip + 1
			data['sIP'] = str(int_ip)
			ipadd[src] = str(int_ip) 
			iplist[src][dst] = str(int_ip)
			count = count + 1
			for snode, sdata in N.nodes(data=True):
				if snode == src and sdata['dtype'] == "server":
					complist[snode].append(str(int_ip))
			int_ip = int_ip + 1
			iplist[dst][src] = str(int_ip)
			data['dIP'] = str(int_ip)
			ipadd[dst] = str(int_ip)
			count = count + 1
			for dnode, ddata in N.nodes(data=True):
				if dnode == dst and ddata['dtype'] == "server":
					complist[dnode].append(str(int_ip))
		if count != 1:
			temp = 255 - (count - 2) 	
			int_ip = int_ip + temp
			count = 1 

	# for src, dst, sdata in N.edges(data=True):
	# 	print src, dst, sdata['sIP'], sdata['dIP'] 
	##################################################################################################################
	##################################################################################################################
	##################################################################################################################
	cdxlist = {}
	cdxlist["cfw"] = [] 

	### TODO: CDX Access Rules for the following:-

	###       allow management server have access to all devices
	fwrule = "access-list vinas-mgt-out permit ip host "+ipadd["management"]+" any"
	cdxlist["cfw"].append(fwrule)
	fwrule = "access-list vinas-mgt-in deny ip any host "+ipadd["management"]
	cdxlist["cfw"].append(fwrule)

	  
	for snode, sdata in N.nodes(data=True):
		if snode in attServers: 
			for dnode, ddata in N.nodes(data=True):
				if snode != dnode:
					if ddata['dtype'] != 'sw' and ddata['cdxpol'] != 'NULL': 
						if sdata['cdxpol'] == ddata['cdxpol']:
							neighs = check_neighbors(snode, dnode)
							if neighs == 'TRUE':
								logging.warning('CDX devices %s and %s behind switch or router', sdata['label'], ddata['label'])
							else:###       attack servers should have access to all devices in their team so as to be able to fix vulnerabilities
								fwrule = "access-list vinas-steam-out permit ip host "+complist[snode][8]+" host "+complist[dnode][8]
								cdxlist["cfw"].append(fwrule)
								fwrule = "access-list vinas-steam-in deny ip host "+complist[dnode][8]+" host "+complist[snode][8]
								cdxlist["cfw"].append(fwrule)			
						else: ###          attack servers should have appropriate access to other teams' vuln servers 
							for sinode, sidata in I.nodes(data=True):
								if sidata['label'] == sdata['cdxpol']:
									for dinode, didata in I.nodes(data=True):
										if didata['label'] == ddata['cdxpol']:
											for src, dst, idata in I.edges(data=True):
												if sinode == src and dinode == dst:
													if idata['label'] == 'comprehensive':
														fwrule = "access-list vinas-out permit ip host "+complist[snode][8]+" host "+complist[dnode][8]
														cdxlist["cfw"].append(fwrule)
														fwrule = "access-list vinas-in permit ip host "+complist[dnode][8]+" host "+complist[snode][8]
														cdxlist["cfw"].append(fwrule)
													elif idata['label'] == 'offensive':
														fwrule = "access-list vinas-out permit ip host "+complist[snode][8]+" host "+complist[dnode][8]
														cdxlist["cfw"].append(fwrule)
														fwrule = "access-list vinas-in deny ip host "+complist[dnode][8]+" host "+complist[snode][8]
														cdxlist["cfw"].append(fwrule)
													elif idata['label'] == 'defensive':
														fwrule = "access-list vinas-out deny ip host "+complist[snode][8]+" host "+complist[dnode][8]
														cdxlist["cfw"].append(fwrule)
														fwrule = "access-list vinas-in permit ip host "+complist[dnode][8]+" host "+complist[snode][8]
														cdxlist["cfw"].append(fwrule)

else:
	asn = {}
	nNames = {}
	bgpint = {}

	for node,data in I.nodes(data=True):
		nNames[node] = data['label']
		asn[data['label']] = data['asn']	
	# print asn


	## Network Dictionaries
	complist = {}
	setasn = {}
	iplist = {}
	cdxcomp = {}
	list = []
	ncount = 0
	noint = 0	
	i = 1

	### DICT 1 - Network Layout Component Details
	# network layout graph details...
	for node,data in N.nodes(data=True):
		ncount = ncount + 1
		complist[node] = []
		complist[node].append(data['label'])
		complist[node].append(ncount)
		complist[node].append(data['dtype'])
		complist[node].append(data['bgppol'])
		complist[node].append(data['fwpol'])
		complist[node].append(data['cdxpol'])
		complist[node].append(noint)
		complist[node].append(i)
	# print complist


	#DICT 3 - Assign IP addresses and netmasks to the various interfaces
	### IP ADDRESS and SUBNET ALLOCATION ###
	for fnode, fdata in N.nodes(data=True):
		iplist[fnode] = {}
		if fdata['dtype'] == 'server' or fdata['dtype'] == 'fw':
			setasn[fnode] = 1
	
	for src,dst,data in N.edges(data=True):
		iplist[src][dst] = 'NULL'
		iplist[dst][src] = 'NULL'	

	count = 1
	#IP Allocation for devices behind switches
	for src,dst,data in N.edges(data=True):
		for node, ndata in N.nodes(data=True):
			if node == src and ndata['dtype'] == 'sw':
				for neighs in nx.all_neighbors(N, src):
					if iplist[src][neighs] == 'NULL':
						int_ip = int_ip + 1
						data['sIP'] = str(int_ip)
						iplist[src][neighs] = str(int_ip)
						count = count + 1
						complist[neighs].append(str(int_ip))
					if iplist[neighs][src] == 'NULL':
						int_ip = int_ip + 1
						data['dIP'] = str(int_ip)
						iplist[neighs][src] = str(int_ip)
						count = count + 1
						complist[neighs].append(str(int_ip))
				temp = 255 - (count - 2)
				int_ip = int_ip + temp
				count = 1 

	count = 1
	#IP Allocation for other topology devices	
	for src,dst,data in N.edges(data=True):
		if iplist[src][dst] == 'NULL':
			int_ip = int_ip + 1
			data['sIP'] = str(int_ip)
			iplist[src][dst] = str(int_ip)
			count = count + 1
			for snode, sdata in N.nodes(data=True):
				if snode == src and sdata['dtype'] == "server":
					complist[snode].append(str(int_ip))
		if iplist[dst][src] == 'NULL':
			int_ip = int_ip + 1
			iplist[dst][src] = str(int_ip)
			count = count + 1
			data['dIP'] = str(int_ip)
			for dnode, ddata in N.nodes(data=True):
				if dnode == dst and ddata['dtype'] == "server":
					complist[dnode].append(str(int_ip))
		if count != 1:
			temp = 255 - (count - 2) 	
			int_ip = int_ip + temp
			count = 1 
			
	# print iplist






##################################################################################################################
##################################################################################################################
##################################################################################################################
################################################### BGP SECTION ##################################################
##################################################################################################################
##################################################################################################################
##################################################################################################################
bgprel = {}
fwrel = {}
bgplist = {}
bgpaccess = {}


for fnode, fdata in N.nodes(data=True):
	if fdata['dtype'] == 'fw':
		fwrel[fdata['label']] = {}
	elif fdata['dtype'] == 'router':
		bgprel[fdata['label']] = {}
		bgplist[fdata['label']] = []
		bgpaccess[fdata['label']] = []

		
for node, data in N.nodes(data=True):
	if data['dtype'] == 'server' or data['dtype'] == 'fw': 
		for src,dst,edata in N.edges(data=True):
			if node == src:
				for dnode, ddata in N.nodes(data=True):
					if dnode == dst:
						if ddata['dtype'] == 'router':
							if ddata['bgppol'] != 'NULL':
								if setasn[node] == 1:
									setasn[node] = str(asn[ddata['bgppol']])
							if ddata['network'] == 'NULL':
								ddata['network'] = str(iplist[src][dst])
							else:
								temp = ddata['network']
								ddata['network'] = temp+', '+str(iplist[src][dst])
						elif ddata['dtype'] == 'fw': 
							for neighs in nx.all_neighbors(N, dnode):
								for cnode, cdata in N.nodes(data=True):
									if cnode == neighs and cdata['dtype'] == 'router':
										if cdata['bgppol'] != 'NULL':
											if setasn[node] == 1:
												setasn[node] = str(asn[ddata['bgppol']])
										if cdata['network'] == 'NULL':
											cdata['network'] = str(iplist[src][dst])
										else:
											temp = cdata['network']
											cdata['network'] = temp+','+str(iplist[src][dst])
						elif ddata['dtype'] == 'sw':
							for neighs in nx.all_neighbors(N, dnode):
								for cnode, cdata in N.nodes(data=True):
									if cnode == neighs and cdata['dtype'] == 'router':
										if cdata['bgppol'] != 'NULL':
											if setasn[node] == 1:
												setasn[node] = str(asn[ddata['bgppol']])
										if cdata['network'] == 'NULL':
											cdata['network'] = str(iplist[src][dst])
										else:
											temp = cdata['network']
											cdata['network'] = temp+','+str(iplist[src][dst])		
									elif cnode == neighs and cdata['dtype'] == 'fw':
										for last in nx.all_neighbors(N, neighs):
											for lnode, ldata in N.nodes(data=True):
												if lnode == last and ldata['dtype'] == 'router':
													if ldata['bgppol'] != 'NULL':
														if setasn[node] == 1:
															setasn[node] = str(asn[ldata['bgppol']])
													if ldata['network'] == 'NULL':
														ldata['network'] = str(iplist[src][dst])
													else:
														temp = ldata['network']
														ldata['network'] = temp+','+str(iplist[src][dst])

# get all customer routes of a router in the network layout
# for src, dst, edata in N.edges(data=True):
# 	for snode, sdata in N.nodes(data=True):
# 		if snode == src and sdata['dtype'] == 'router':
# 			for dnode, ddata in N.nodes(data=True):
# 				if dnode == dst and ddata['dtype'] == 'router':
# 					for isrc, idst, iedata in I.edges(data=True):
# 						iedata['label'] == 'customer':
# 							for sinode, sidata in I.nodes(data=True):
# 								if sdata['bgppol'] == sidata['label'] and sidnode == isrc:
# 									for dinode, didata in I.nodes(data=True):
# 										if ddata['bgppol'] == didata['label'] and didnode == idst:
# 											add asn number to custRoute
				
# print complist
				
for src,dst,edata in N.edges(data=True): 
	for node, data in N.nodes(data=True):
		for dnode, ddata in N.nodes(data=True):
			if node != dnode:
				if data['bgppol'] != 'NULL' and ddata['bgppol'] != 'NULL':
					for sinode, sidata in I.nodes(data=True):
						for dinode, didata in I.nodes(data=True):
							if data['bgppol'] == sidata['label'] and ddata['bgppol'] == didata['label']:
								if data['bgppol'] == ddata['bgppol']:
									logging.warning('%s %s %s %s Same realm hence link will be iBGP', data['label'], data['bgppol'], ddata['label'], ddata['bgppol'])
								else:
									for isrc,idst,idata in I.edges(data=True):
										if node == src and dnode == dst:
											if idata['label'] == 'sibling' or idata['label'] == 'provider' or idata['label'] == 'customer' or idata['label'] == 'peer' or idata['label'] == 'hybrid':
												if sinode == isrc and dinode == idst:
													if idata['label'] == 'sibling' or idata['label'] == 'provider':
														rule,access = export_allroutes(str(iplist[dnode][node]), str(asn.get(ddata['bgppol'])))
														bgplist[data['label']].append(rule)
														bgpaccess[data['label']].append(access)
													elif idata['label'] == 'customer' or  idata['label'] == 'peer':
														rule, access = export_custroutes(str(iplist[node][dnode]), str(iplist[dnode][node]), str(asn.get(ddata['bgppol'])), data['network'])
														bgplist[data['label']].append(rule)
														bgpaccess[data['label']].append(access)
													elif idata['label'] == 'hybrid':
														if idata['label'] == 'sibling' or idata['label'] == 'provider':
															rule,access = export_allroutes(str(iplist[dnode][node]), str(asn.get(ddata['bgppol'])))
															bgplist[data['label']].append(rule)
															bgpaccess[data['label']].append(access)
														elif idata['label'] == 'customer' or  idata['label'] == 'peer':
															rule, access = export_custroutes(str(iplist[node][dnode]), str(iplist[dnode][node]), str(asn.get(ddata['bgppol'])), data['network'])
															bgplist[data['label']].append(rule)
															bgpaccess[data['label']].append(access)
												elif sinode == idst and dinode == isrc:
													if idata['label'] == 'sibling' or idata['label'] == 'customer':
														rule,access = export_allroutes(str(iplist[dnode][node]), str(asn.get(ddata['bgppol'])))
														bgplist[data['label']].append(rule)
														bgpaccess[data['label']].append(access)
													elif idata['label'] == 'provider' or  idata['label'] == 'peer':
														rule, access = export_custroutes(str(iplist[node][dnode]), str(iplist[dnode][node]), str(asn.get(ddata['bgppol'])), data['network'])
														bgplist[data['label']].append(rule)
														bgpaccess[data['label']].append(access)
													elif idata['label'] == 'hybrid':
														if idata['label'] == 'sibling' or idata['label'] == 'customer':
															rule,access = export_allroutes(str(iplist[dnode][node]), str(asn.get(ddata['bgppol'])))
															bgplist[data['label']].append(rule)
															bgpaccess[data['label']].append(access)
														elif idata['label'] == 'provider' or  idata['label'] == 'peer':
															rule, access = export_custroutes(str(iplist[node][dnode]), str(iplist[dnode][node]), str(asn.get(ddata['bgppol'])), data['network'])
															bgplist[data['label']].append(rule)
															bgpaccess[data['label']].append(access)
												else: 
													logging.warning('%s %s %s %s Realms have no relationship', data['label'], data['bgppol'], ddata['label'], ddata['bgppol'])
										elif node == dst and dnode == src:
											if idata['label'] == 'sibling' or idata['label'] == 'provider' or idata['label'] == 'customer' or idata['label'] == 'peer' or idata['label'] == 'hybrid':
												if sinode == isrc and dinode == idst:
													if idata['label'] == 'sibling' or idata['label'] == 'provider':
														rule,access = export_allroutes(str(iplist[dnode][node]), str(asn.get(ddata['bgppol'])))
														bgplist[data['label']].append(rule)
														bgpaccess[data['label']].append(access)
													elif idata['label'] == 'customer' or  idata['label'] == 'peer':
														rule, access = export_custroutes(str(iplist[node][dnode]), str(iplist[dnode][node]), str(asn.get(ddata['bgppol'])), data['network'])
														bgplist[data['label']].append(rule)
														bgpaccess[data['label']].append(access)
													elif idata['label'] == 'hybrid':
														if idata['label'] == 'sibling' or idata['label'] == 'provider':
															rule,access = export_allroutes(str(iplist[dnode][node]), str(asn.get(ddata['bgppol'])))
															bgplist[data['label']].append(rule)
															bgpaccess[data['label']].append(access)
														elif idata['label'] == 'customer' or  idata['label'] == 'peer':
															rule, access = export_custroutes(str(iplist[node][dnode]), str(iplist[dnode][node]), str(asn.get(ddata['bgppol'])), data['network'])
															bgplist[data['label']].append(rule)
															bgpaccess[data['label']].append(access)
												elif sinode == idst and dinode == isrc:
													if idata['label'] == 'sibling' or idata['label'] == 'customer':
														rule,access = export_allroutes(str(iplist[dnode][node]), str(asn.get(ddata['bgppol'])))
														bgplist[data['label']].append(rule)
														bgpaccess[data['label']].append(access)
													elif idata['label'] == 'provider' or  idata['label'] == 'peer':
														rule, access = export_custroutes(str(iplist[node][dnode]), str(iplist[dnode][node]), str(asn.get(ddata['bgppol'])), data['network'])
														bgplist[data['label']].append(rule)
														bgpaccess[data['label']].append(access)
													elif idata['label'] == 'hybrid':
														if idata['label'] == 'sibling' or idata['label'] == 'customer':
															rule,access = export_allroutes(str(iplist[dnode][node]), str(asn.get(ddata['bgppol'])))
															bgplist[data['label']].append(rule)
															bgpaccess[data['label']].append(access)
														elif idata['label'] == 'provider' or  idata['label'] == 'peer':
															rule, access = export_custroutes(str(iplist[node][dnode]), str(iplist[dnode][node]), str(asn.get(ddata['bgppol'])), data['network'])
															bgplist[data['label']].append(rule)
															bgpaccess[data['label']].append(access)
												else:
													logging.warning('%s %s %s %s Realms have no relationship', data['label'], data['bgppol'], ddata['label'], ddata['bgppol'])



##################################################################################################################
##################################################################################################################
##################################################################################################################
################################################# FIREWALL SECTION ###############################################
##################################################################################################################
##################################################################################################################
##################################################################################################################
N = N.to_undirected()
ftable = {}
fwlist = {}
listfw = {}
anyrealm = []

for node, data in N.nodes(data=True):
	if data['fwpol'] != 'NULL':
		ftable[data['label']] = {}
		listfw[data['label']] = []

for fnode, fdata in N.nodes(data=True):
	if fdata['dtype'] == 'fw':
		fwlist[fdata['label']] = []

## Capture all Realms that have an ANY relationship in the intent graph
##################################################################################################################
for node,data in I.nodes(data=True):
	for dnode,ddata in I.nodes(data=True):
		if data['label'] == 'any':
			for src,dst,edata in I.edges(data=True):
				if node == src and dnode == dst:
					if ddata['label'] not in anyrealm:
						anyrealm.append(ddata['label'])
				elif node == dst and dnode == src:
					if ddata['label'] not in anyrealm:
						anyrealm.append(ddata['label'])

for node,data in I.nodes(data=True):
	for src,dst,edata in I.edges(data=True):
		if node == src and node == dst:
			if data['label'] not in anyrealm: 
				anyrealm.append(data['label'])		
		                                                                                              
for node, data in N.nodes(data=True):
	for dnode, ddata in N.nodes(data=True):
		if data['fwpol'] != 'NULL' and ddata['fwpol'] != 'NULL' and node != dnode:
			for path in nx.all_simple_paths(N, source=node, target=dnode):
				for p in path: 
					for fnode, fdata in N.nodes(data=True):
						if fnode == p and fdata['dtype'] == 'fw':
							if ddata['label'] in ftable[data['label']].keys(): 
								if fdata['label'] not in ftable[data['label']][ddata['label']]:
									ftable[data['label']][ddata['label']].append(fdata['label'])
							else:
								ftable[data['label']][ddata['label']] = []
								ftable[data['label']][ddata['label']].append(fdata['label'])


for node, data in N.nodes(data=True):
	if data['fwpol'] != 'NULL' and data['fwpol'] in anyrealm:
		for neighs in nx.all_neighbors(N, node):
			for cnode, cdata in N.nodes(data=True):
				if cnode == neighs and cdata['dtype'] == 'fw':
					listfw[data['label']] = cdata['label']
				elif cnode == neighs and cdata['dtype'] == 'sw':
					for sneighs in nx.all_neighbors(N, neighs):
						for snode, sdata in N.nodes(data=True):
							if snode == sneighs and sdata['dtype'] == 'fw':
								listfw[data['label']] = sdata['label']
				elif cnode == neighs and cdata['dtype'] == 'sw':
					for next in nx.all_neighbors(N, node):
						for ccnode, ccdata in N.nodes(data=True):
							if ccnode == next and ccdata['dtype'] == 'fw':
								listfw[data['label']] = ccdata['label']
							elif cnode == neighs and cdata['dtype'] == 'sw':
								for sneighs in nx.all_neighbors(N, neighs):
									for snode, sdata in N.nodes(data=True):
										if snode == sneighs and sdata['dtype'] == 'fw':
											listfw[data['label']] = sdata['label']


# Realm-to-External network or host Implementation
for node, data in N.nodes(data=True):
	if data['fwpol'] != 'NULL':
		for inode, idata in I.nodes(data=True):
			if idata['label'] == data['fwpol']:
				for esrc, edst, edata in I.edges(data=True):
					if inode == esrc and inode == edst:
						diffprotos = edata['protocol'].split(',')
						for protocol in diffprotos:
							diffservices = edata['service'].split(',')
							for service in diffservices:
								p = listfw[data['label']]
								if service == 'NULL':
									if idata['sPort'] == 'NULL':
										fwrule = "access-list vinas-out "+str(edata['label'])+" "+str(protocol)+" host "+str(complist[node][8])+" host "+str(edata['dest'])
										fwlist[p].append(fwrule)
									else:
										fwrule = "access-list vinas-out "+str(edata['label'])+" "+str(protocol)+" host "+str(complist[node][8])+" eq "+str(idata['sPort'])+" host "+str(edata['dest'])
										fwlist[p].append(fwrule)
								else:
									fwrule = "access-list vinas-out "+str(edata['label'])+" "+str(protocol)+" host "+str(complist[node][8])+" host "+str(edata['dest'])+" eq "+service
									fwlist[p].append(fwrule)
	
# Realm-to-Realm Implementation
for node, data in N.nodes(data=True):
	if data['fwpol'] != 'NULL':
		for dnode, ddata in N.nodes(data=True):
			if ddata['fwpol'] != 'NULL':
				if node != dnode:
					neighs = check_neighbors(node, dnode)
					if neighs == 'TRUE':
						logging.warning('No firewall rule because %s and %s behind switch or router', data['label'], ddata['label'])
					else:		
						diffrealms = data['fwpol'].split(',')
						for realm in diffrealms:
							for sinode, sidata in I.nodes(data=True):
								for dinode, didata in I.nodes(data=True):
									if realm == sidata['label'] and ddata['fwpol'] == didata['label']:
										if sidata['label'] == didata['label']:
											logging.warning('Devices in the same realm will have no policy applied!')
										else:
											for isnode, idnode, idata in I.edges(data=True):
												if sinode == isnode and dinode == idnode:
													if idata['label'] == 'deny':
														diffprotos = idata['protocol'].split(',')
														for protocol in diffprotos:
															diffservices = idata['service'].split(',')
															for service in diffservices:
																p = ftable[data['label']][ddata['label']][0]
																if service == 'NULL':
																	if idata['sPort'] == 'NULL':
																		fwrule = "access-list vinas-out "+str(idata['label'])+" "+str(protocol)+" host "+str(complist[node][8])+" host "+str(complist[dnode][8])
																		fwlist[p].append(fwrule)
																	else:
																		fwrule = "access-list vinas-out "+str(idata['label'])+" "+str(protocol)+" host "+str(complist[node][8])+" eq "+str(idata['sPort'])+" host "+str(complist[dnode][8])
																		fwlist[p].append(fwrule)
																else:
																	fwrule = "access-list vinas-out "+str(idata['label'])+" "+str(protocol)+" host "+str(complist[node][8])+" host "+str(complist[dnode][8])+" eq "+service
																	fwlist[p].append(fwrule) 
													elif idata['label'] == 'permit':  # the [0]th of the firewall will be vinas-out while the remaining will be vinas-in 
														diffprotos = idata['protocol'].split(',')
														for protocol in diffprotos:
															diffservices = idata['service'].split(',')
															for service in diffservices:
																fws = ftable[data['label']][ddata['label']]
																firewall = fws[0]
																firewalls = fws[1:]
																if service == 'NULL':
																	if idata['sPort'] == 'NULL':
																		fwrule = "access-list vinas-out "+str(idata['label'])+" "+str(protocol)+" host "+str(complist[node][8])+" host "+str(complist[dnode][8])
																		fwlist[firewall].append(fwrule)
																	else:
																		fwrule = "access-list vinas-out "+str(idata['label'])+" "+str(protocol)+" host "+str(complist[node][8])+" eq "+str(idata['sPort'])+" host "+str(complist[dnode][8])
																		fwlist[firewall].append(fwrule)
																else:
																	fwrule = "access-list vinas-out "+str(idata['label'])+" "+str(protocol)+" host "+str(complist[node][8])+" host "+str(complist[dnode][8])+" eq "+service
																	fwlist[firewall].append(fwrule)
																for p in firewalls:
																	if service == 'NULL':
																		if idata['sPort'] == 'NULL':
																			fwrule = "access-list vinas-in "+str(idata['label'])+" "+str(protocol)+" host "+str(complist[node][8])+" host "+str(complist[dnode][8])
																			fwlist[p].append(fwrule)
																		else:
																			fwrule = "access-list vinas-in "+str(idata['label'])+" "+str(protocol)+" host "+str(complist[node][8])+" eq "+str(idata['sPort'])+" host "+str(complist[dnode][8])
																			fwlist[p].append(fwrule)
																	else:
																		fwrule = "access-list vinas-in "+str(idata['label'])+" "+str(protocol)+" host "+str(complist[node][8])+" host "+str(complist[dnode][8])+" eq "+service
																		fwlist[p].append(fwrule)

# Realm-to-Any or Any-to-Realm Implementation						
for node, data in N.nodes(data=True):
	if data['fwpol'] != 'NULL' and data['fwpol'] in anyrealm:
		for snode, sdata in I.nodes(data=True):
			for dinode, didata in I.nodes(data=True):
				for isrc, idst, idata in I.edges(data=True):
					if snode == isrc and dinode == idst:
						if data['fwpol'] == sdata['label'] and didata['label'] == 'any':
							diffprotos = idata['protocol'].split(',')
							for protocol in diffprotos:
								diffservices = idata['service'].split(',')
								for service in diffservices:
									p = listfw[data['label']]
									if service == 'NULL':
										if idata['sPort'] == 'NULL':
											fwrule = "access-list vinas-out "+str(idata['label'])+" "+str(protocol)+" host "+str(complist[node][8])+" any "
											fwlist[p].append(fwrule)
										else:
											fwrule = "access-list vinas-out "+str(idata['label'])+" "+str(protocol)+" host "+str(complist[node][8])+" eq "+str(idata['sPort'])+" any "
											fwlist[p].append(fwrule)
									else:
										fwrule = "access-list vinas-out "+str(idata['label'])+" "+str(protocol)+" host "+str(complist[node][8])+" any eq "+service
										fwlist[p].append(fwrule)
					elif snode == idst and dinode == isrc:
						if data['fwpol'] == sdata['label'] and didata['label'] == 'any':
							diffprotos = idata['protocol'].split(',')
							for protocol in diffprotos:
								diffservices = idata['service'].split(',')
								for service in diffservices:
									for fnode, fdata in N.nodes(data=True):	
										if fdata['dtype'] == 'fw':									
											if service == 'NULL':
												if idata['sPort'] == 'NULL':
													fwrule = "access-list vinas-in "+str(idata['label'])+" "+str(protocol)+" any host "+str(complist[node][8])
													fwlist[fdata['label']].append(fwrule)
												else:
													fwrule = "access-list vinas-in "+str(idata['label'])+" "+str(protocol)+" any eq "+str(idata['sPort'])+" host "+str(complist[node][8])
													fwlist[fdata['label']].append(fwrule)
											else:
												fwrule = "access-list vinas-in "+str(idata['label'])+" "+str(protocol)+" any host "+str(complist[node][8])+" eq "+service
												fwlist[fdata['label']].append(fwrule)

# Any-to-Any Rule Implementation
for node, data in N.nodes(data=True):
	if data['dtype'] == 'fw':
		for snode, sdata in I.nodes(data=True):
			for dnode, ddata in I.nodes(data=True):
				if ddata['label'] == 'any' and sdata['label'] == 'any':
					for isrc, idst, idata in I.edges(data=True):
						if snode==isrc and dnode==idst:
							diffprotos = idata['protocol'].split(',')
							for protocol in diffprotos:
								diffservices = idata['service'].split(',')
								for service in diffservices:
									if service == 'NULL':
										if idata['sPort'] == 'NULL':
											fwrule = "access-list vinas-out "+str(idata['label'])+" "+str(protocol)+" any any"
											fwlist[data['label']].append(fwrule)
											inrule = "access-list vinas-in "+str(idata['label'])+" "+str(protocol)+" any any"
											fwlist[data['label']].append(inrule)
										else:
											fwrule = "access-list vinas-out "+str(idata['label'])+" "+str(protocol)+" any eq "+str(idata['sPort'])+" any "
											fwlist[data['label']].append(fwrule)
											inrule = "access-list vinas-in "+str(idata['label'])+" "+str(protocol)+" any eq "+str(idata['sPort'])+" any "
											fwlist[data['label']].append(inrule)
									else:
										fwrule = "access-list vinas-out "+str(idata['label'])+" "+str(protocol)+" any any eq "+service
										fwlist[data['label']].append(fwrule)
										inrule = "access-list vinas-in "+str(idata['label'])+" "+str(protocol)+" any any eq "+service
										fwlist[data['label']].append(inrule)

##################################################################################################################
# merge or assign data structure of respective network component from network graph designed by the user
# print cdxcomp
# print ""
# print fwlist
# print ""
# for node,data in N.nodes(data=True):
# 	if data['network'] != 'NULL':
# 		print data['label'], data['network']


##################################################################################################################
##################################################################################################################
##################################################################################################################
### BUILIDING VIRL VIA LXML	FILE ###		
##################################################################################################################
##################################################################################################################
##################################################################################################################
temp = 1
### TASK 1 - Nodes and Interfaces
for u,n in N.nodes(data=True): 
	if n['dtype'] == "server":
		node = etree.SubElement(topology, "node",
								name = n['label'],
								type = "SIMPLE",
								subtype = check_device(n['dtype']),
								location = str(int(float(n['x'])))+","+str(int(float(n['y'])))
		)
		extensions = etree.SubElement(node, "extensions")
		entry = etree.SubElement(extensions, "entry",
									 key = "AutoNetkit.ASN",
									 type = "Integer"
									 )
		entry.text = str(setasn[u])
		for a,b,c in N.edges(nbunch=[u],data=True):
			if a == u :
				complist[a][7] = complist[a][7] + 1
				complist[a][6] = complist[a][6] + 1
				if c['sInt_id'] == 'NULL':
					c['sInt_id'] = complist[a][6]
				else:
					c['dInt_id'] = complist[a][6]
				interface = etree.SubElement(node, "interface", id = str(complist[a][6]), name="eth"+str(complist[a][7]), ipv4 = allocate_ip(a,b), netPrefixLenV4="24")
			elif b == u :
				print "yes"
				complist[a][6] = complist[a][6] + 1
				interface = etree.SubElement(node, "interface", id = str(complist[a][6]), name="eth"+str(complist[a][7]), ipv4 = allocate_ip(a,b), netPrefixLenV4="24")	
	elif n['dtype'] == "fw":
		node = etree.SubElement(topology, "node",
								name = n['label'],
								type = "SIMPLE",
								subtype = check_device(n['dtype']),
								location = str(int(float(n['x'])))+","+str(int(float(n['y'])))
		)
		extensions = etree.SubElement(node, "extensions")
		entry = etree.SubElement(extensions, "entry",
									 key = "AutoNetkit.custom_config_global",
									 type = "string"
									 )
		entry.text = custom_fwconfig(n['label'])
		entry = etree.SubElement(extensions, "entry",
									 key = "AutoNetkit.ASN",
									 type = "Integer"
									 )
		entry.text = str(setasn[u])
		for a,b,c in N.edges(nbunch=[u],data=True):
			if a == u :
				complist[a][7] = complist[a][7] + 1
				complist[a][6] = complist[a][6] + 1
				if c['sInt_id'] == 'NULL':
					c['sInt_id'] = complist[a][6]
				else:
					c['dInt_id'] = complist[a][6]
				interface = etree.SubElement(node, "interface", id = str(complist[a][6]), name="GigabitEthernet0/"+str(complist[a][6]), ipv4 = allocate_ip(a,b), netPrefixLenV4="24")
			elif b == u :
				print "yes"
				complist[a][6] = complist[a][6] + 1
				interface = etree.SubElement(node, "interface", id = str(complist[a][6]), name="GigabitEthernet0/"+str(complist[a][6]), ipv4 = allocate_ip(a,b), netPrefixLenV4="24")	
	elif n['dtype'] == "sw":
		node = etree.SubElement(topology, "node",
								name = n['label'],
								type = "SIMPLE",
								subtype = check_device(n['dtype']),
								location = str(int(float(n['x'])))+","+str(int(float(n['y'])))
		)
		for a,b,c in N.edges(nbunch=[u],data=True):
			if a == u :
				complist[a][7] = complist[a][7] + 1
				complist[a][6] = complist[a][6] + 1
				if c['sInt_id'] == 'NULL':
					c['sInt_id'] = complist[a][6]
				else:
					c['dInt_id'] = complist[a][6]
				interface = etree.SubElement(node, "interface", id = str(complist[a][6]), name = "link"+str(complist[a][7]))
			elif b == u :
				print "yes"
				complist[a][6] = complist[a][6] + 1
				interface = etree.SubElement(node, "interface", id = str(complist[a][6]), name = "link"+str(complist[a][7])) 
	else:
		lbcount = lbcount + 1
		node = etree.SubElement(topology, "node",
								name = n['label'],
								type = "SIMPLE",
								subtype = check_device(n['dtype']),
								location = str(int(float(n['x'])))+","+str(int(float(n['y']))),
								ipv4 = allocate_loopback(n['dtype'], lbcount)
		)
		if n['bgppol'] != 'NULL':
			extensions = etree.SubElement(node, "extensions")
			entry = etree.SubElement(extensions, "entry",
									 key = "AutoNetkit.ASN",
									 type = "Integer"
									 )
			entry.text = assign_bgp(n['bgppol'])
			entry = etree.SubElement(extensions, "entry",
										 key = "AutoNetkit.custom_config_global",
										 type = "string"
										 )
			entry.text = bgp_globalconfig(n['label'])
			entry = etree.SubElement(extensions, "entry",
									 key = "AutoNetkit.custom_config_bgp",
									 type = "string"
									 )
			entry.text = custom_bgpconfig(n['network'], n['label'])
		for a,b,c in N.edges(nbunch=[u],data=True):
			if a == u:
				complist[a][7] = complist[a][7] + 1
				complist[a][6] = complist[a][6] + 1
				if c['sInt_id'] == 'NULL':
					c['sInt_id'] = complist[a][6]
				else:
					c['dInt_id'] = complist[a][6]
				interface = etree.SubElement(node, "interface", id = str(complist[a][6]), name="GigabitEthernet0/"+str(complist[a][7]), ipv4 = allocate_ip(a,b), netPrefixLenV4="24")
			elif b == u : 
				print "yes"
				complist[a][6] = complist[a][6] + 1
				interface = etree.SubElement(node, "interface", id = str(complist[a][6]), name="GigabitEthernet0/"+str(complist[a][7]), ipv4 = allocate_ip(a,b), netPrefixLenV4="24")			     	
##################################################################################################################
### TASK 2 - Connections between devices in XML
annotations = etree.SubElement(topology, "annotations")
for src,dst,data in N.edges(data=True):
	if data['sInt_id'] == 'NULL' or data['dInt_id'] == 'NULL':
		logging.warning('One of your interface ids is NULL')
	else:
		connection = etree.SubElement(topology, "connection", 
									  dst = "/virl:topology/virl:node["+str(complist[src][1])+"]/virl:interface["+str(data['sInt_id'])+"]", 
								 	  src = "/virl:topology/virl:node["+str(complist[dst][1])+"]/virl:interface["+str(data['dInt_id'])+"]"
									  )


               
print(etree.tostring(topology, pretty_print=True))